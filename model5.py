import argparse
import datetime as dt
import math
import pickle
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import sqlite3
import logging
import numpy as np
import hashlib
import os
from tqdm import tqdm
from multiprocessing import Event, Process, Manager
from queue import Queue
from data import (
    length,
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    GENERATE_READABLE,
    convert_program_to_one_hot,
    subsumes,
    prog_subsumes,
    index_to_prog,
    index_to_rule,
)
from torch.utils.tensorboard import SummaryWriter


num_vars = 7
dataset_name = 'model5_dataset_004_2.db'
#train_size = int(5000*1024)
train_size = int(500*1024)
val_size = 1024
test_size = 1024
do_generate = False
do_stats = False
do_params = False
do_train = True
do_test = True
has_s3 = False

tuple_length = 2
train_epochs = 10
batch_size = 128
eval_batch_size = 16

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('model5')
logger.addHandler(logging.FileHandler(f'{time.strftime("%Y%m%d-%H%M")}.log', mode='w'))
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

half_point = math.ceil(num_vars/2)
#max_rule_len = length(half_point, num_vars) - length(half_point-1, num_vars)
max_rule_len = length(5, num_vars) - length(4, num_vars)

writer = SummaryWriter(f'runs/exp_var_{num_vars}_{dt.datetime.now():%Y%m%d%H%M%S}')


def convert_state_to_numbers(state):
    return sum([s * (2**i) for i, s in enumerate(state)])


class MAB(nn.Module):
    def __init__(self, dim_Q, dim_K, dim_V, num_heads, ln=False):
        super(MAB, self).__init__()
        self.dim_V = dim_V
        self.num_heads = num_heads
        self.fc_q = nn.Linear(dim_Q, dim_V)
        self.fc_k = nn.Linear(dim_K, dim_V)
        self.fc_v = nn.Linear(dim_K, dim_V)
        self.ln0 = None
        self.ln1 = None
        if ln:
            self.ln0 = nn.LayerNorm(dim_V)
            self.ln1 = nn.LayerNorm(dim_V)
        self.fc_o = nn.Linear(dim_V, dim_V)

    def forward(self, Q, K):
        Q = self.fc_q(Q)
        K, V = self.fc_k(K), self.fc_v(K)

        dim_split = self.dim_V // self.num_heads
        Q_ = torch.cat(Q.split(dim_split, 2), 0)
        K_ = torch.cat(K.split(dim_split, 2), 0)
        V_ = torch.cat(V.split(dim_split, 2), 0)

        A = torch.softmax(Q_.bmm(K_.transpose(1, 2)) / math.sqrt(self.dim_V), 2)
        O = torch.cat((Q_ + A.bmm(V_)).split(Q.size(0), 0), 2)
        O = O if self.ln0 is None else self.ln0(O)
        O = O + F.relu(self.fc_o(O))
        O = O if self.ln1 is None else self.ln1(O)
        return O


class SAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, ln=False):
        super(SAB, self).__init__()
        self.mab = MAB(dim_in, dim_in, dim_out, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(X, X)


class ISAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, num_inds, ln=False):
        super(ISAB, self).__init__()
        self.I = nn.Parameter(torch.Tensor(1, num_inds, dim_out))
        nn.init.xavier_uniform_(self.I)
        self.mab0 = MAB(dim_out, dim_in, dim_out, num_heads, ln=ln)
        self.mab1 = MAB(dim_in, dim_out, dim_out, num_heads, ln=ln)

    def forward(self, X):
        H = self.mab0(self.I.repeat(X.size(0), 1, 1), X)
        return self.mab1(X, H)


class PMA(nn.Module):
    def __init__(self, dim, num_heads, num_seeds, ln=False):
        super(PMA, self).__init__()
        self.S = nn.Parameter(torch.Tensor(1, num_seeds, dim))
        nn.init.xavier_uniform_(self.S)
        self.mab = MAB(dim, dim, dim, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(self.S.repeat(X.size(0), 1, 1), X)


class InputEmbeddings(nn.Module):
    def __init__(self, dim_in, dim_out):
        super(InputEmbeddings, self).__init__()
        self.activation = nn.ReLU()
        self.ln = nn.LayerNorm(dim_out)
        self.embed = nn.Embedding(dim_in, dim_out)

    def forward(self, X):
        return self.ln(self.activation(self.embed(X)))


class FFResidual(nn.Module):
    def __init__(self, dim_in, dim_hidden):
        super(FFResidual, self).__init__()
        self.activation = nn.ReLU()
        self.ff = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(dim_in, dim_hidden),
            self.activation,
            nn.Dropout(0.2),
            nn.Linear(dim_hidden, dim_in),
        )
        self.ln = nn.LayerNorm(dim_in)

    def forward(self, X):
        return self.ln(self.ff(X) + X)


class SetTransformer(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output, num_inds=32, dim_hidden=128, num_heads=4, ln=False):
        super(SetTransformer, self).__init__()
        self.st_dim = 128
        self.st_var = 128
        st_dim_hidden = 512
        var_idx_dim = 64
        self.enc = nn.Sequential(
            #ISAB(self.st_dim, st_dim_hidden, num_heads, num_inds, ln=ln),
            SAB(self.st_dim, st_dim_hidden, num_heads, ln=ln),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #SAB(32, dim_hidden, num_heads, ln=ln),
            #SAB(dim_hidden, dim_hidden, num_heads, ln=ln)
        )
        self.dec = nn.Sequential(
            PMA(st_dim_hidden, num_heads, num_outputs, ln=ln),
            nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            nn.Dropout(0.2),
            nn.Linear(st_dim_hidden, self.st_var),
        )
        self.state_ln = nn.LayerNorm(self.st_var)
        self.activation = nn.ReLU()
        self.state_embed_ff = nn.Linear(self.st_dim, self.st_dim)
        self.state_embed_ff_2 = nn.Sequential(
            nn.Linear(self.st_dim, 128),
            self.activation,
            nn.Dropout(0.2),
            nn.Linear(128, self.st_var + var_idx_dim),
        )
        self.rule_ff_2 = nn.Sequential(
            nn.Linear(self.st_var + var_idx_dim, 128),
            self.activation,
            nn.Dropout(0.2),
            nn.Linear(128, self.st_var + var_idx_dim),
        )
        self.var_idx_ff_2 = nn.Sequential(
            nn.Linear(var_idx_dim, 128),
            self.activation,
            nn.Dropout(0.2),
            nn.Linear(128, self.st_var + var_idx_dim),
        )
        self.var_idx_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_embed_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_ln = nn.LayerNorm(self.st_var)
        self.state_mha = nn.MultiheadAttention(self.st_var, num_heads, dropout=0.2)
        self.rule_len_embed = InputEmbeddings(max_rule_len*2, self.st_var + var_idx_dim)
        self.rule_ff = nn.Linear(self.st_var + var_idx_dim, self.st_var + var_idx_dim)
        self.state_embed = InputEmbeddings(2 ** num_vars * 2, self.st_dim)
        self.var_idx_embed = InputEmbeddings(num_vars, var_idx_dim)
        self.var_idx_ff = nn.Linear(var_idx_dim, var_idx_dim)
        self.var_idx_ln = nn.LayerNorm(var_idx_dim)
        self.prog_ff = FFResidual(self.st_var + var_idx_dim, 128*8)
        #self.rule_mha = nn.MultiheadAttention(self.st_var + var_idx_dim, num_heads, dropout=0.1)
        self.rule_mha = MAB(self.st_var + var_idx_dim, self.st_var + var_idx_dim, self.st_var + var_idx_dim, num_heads, ln)
        self.rule_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_ff = FFResidual(self.st_var + var_idx_dim, dim_hidden)
        self.rule_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.rule_ln_3 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.prog_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.prog_pred = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(self.st_var + var_idx_dim, dim_hidden),
            self.activation,
            nn.Dropout(0.2),
            nn.Linear(dim_hidden, dim_output),
        )

    def forward(self, X, rule_len, var_idx):
        rule_len_i = rule_len
        #print(X[0])
        reshape_X_orig = self.state_embed(X)
        reshape_X = self.state_embed_ff(reshape_X_orig)
        encoded = self.enc(reshape_X)
        state = self.state_ln(self.activation(self.dec(encoded)))
        var_idx = self.var_idx_embed(var_idx)
        orig_var_idx = var_idx
        var_idx = self.var_idx_ff(var_idx)
        var_idx = self.var_idx_ln(var_idx + orig_var_idx).unsqueeze(1)
        state = self.state_ff(torch.cat((var_idx, state), 2))
        rule_len = torch.unsqueeze(rule_len, 1)
        rule_len = self.rule_len_embed(rule_len)
        orig_rule_len = rule_len
        rule_len = self.rule_ff(rule_len)
        rule_len = self.rule_ln_2(rule_len + orig_rule_len)
        inp = self.rule_mha(rule_len, state)
        inp = self.rule_ln(inp + rule_len)
        old_inp = inp
        inp = self.prog_ff(inp)
        inp = self.prog_ln(inp + old_inp)
        states_X = self.state_embed_ff_2(reshape_X_orig)
        states_X = torch.max(states_X, 1, keepdim=True).values
        #states_X = torch.sum(states_X, 1, keepdim=True)
        inp = self.state_embed_ln_2(inp + states_X)
        var_idx = self.var_idx_ff_2(orig_var_idx).unsqueeze(1)
        rule_len = self.rule_ff_2(orig_rule_len)
        rule_len = self.var_idx_ln_2(rule_len + var_idx)
        inp = self.rule_ln_3(inp + rule_len)
        #print(inp)
        output = self.prog_pred(inp)
        #print(output)
        return output

class TransformerModel(nn.Module):
    def __init__(self, ninp, nhead, nhid, nlayers, dropout=0.1):
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        super(TransformerModel, self).__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.ninp = ninp
        self.decoder = nn.Linear(ninp, ninp)
        self.init_weights()
        self.src_mask = None

    def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self):
        initrange = 0.1
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src):
        if self.src_mask is None or self.src_mask.size(0) != src.size(0):
            device = src.device
            mask = self.generate_square_subsequent_mask(src.size(0)).to(device)
            self.src_mask = mask

        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, self.src_mask)
        output = self.decoder(output)
        return output

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        pe = torch.zeros(5000, d_model)
        position = torch.arange(0, 5000, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

class SetTransformer2(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output, num_inds=32, dim_hidden=128, num_heads=4, ln=False):
        super(SetTransformer, self).__init__()
        self.st_dim = 128
        self.st_var = 128
        st_dim_hidden = 512
        var_idx_dim = 64
        self.transformer = TransformerModel(self.st_dim, num_heads, st_dim_hidden, 6)
        self.state_ln = nn.LayerNorm(self.st_var)
        self.activation = nn.GELU()
        self.state_ln = nn.LayerNorm(self.st_var)
        self.state_mha = nn.MultiheadAttention(self.st_var, num_heads, dropout=0.1)
        self.rule_len_embed = InputEmbeddings(max_rule_len*2, self.st_var + var_idx_dim)
        self.rule_ff = FFResidual(self.st_var + var_idx_dim, 128)
        self.state_embed = InputEmbeddings(2 ** num_vars * 2, self.st_dim)
        self.var_idx_embed = InputEmbeddings(num_vars, var_idx_dim)
        self.var_idx_ff = FFResidual(var_idx_dim, 64)
        self.var_idx_ln = nn.LayerNorm(var_idx_dim)
        self.prog_ff = FFResidual(self.st_var + var_idx_dim, 128)
        #self.rule_mha = nn.MultiheadAttention(self.st_var + var_idx_dim, num_heads, dropout=0.1)
        self.rule_mha = MAB(self.st_var + var_idx_dim, self.st_var + var_idx_dim, self.st_var + var_idx_dim, num_heads, ln)
        self.rule_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_ff = FFResidual(self.st_var + var_idx_dim, dim_hidden)
        self.rule_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.prog_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.prog_pred = {}
        for i in range(1, num_vars+1):
            self.prog_pred[i] = nn.Sequential(
                nn.Dropout(0.1),
                nn.Linear(self.st_var + var_idx_dim, dim_hidden),
                self.activation,
                nn.Dropout(0.1),
                nn.Linear(dim_hidden, dim_hidden),
                self.activation,
                nn.Dropout(0.1),
                nn.Linear(dim_hidden, dim_hidden),
                self.activation,
                nn.Dropout(0.1),
                nn.Linear(dim_hidden, dim_output),
            )

    def forward(self, X, rule_len, var_idx):
        #print(X[0])
        reshape_X = self.state_embed(X)
        #encoded = self.enc(reshape_X)
        #state = self.state_ln(self.activation(self.dec(encoded)))
        reshape_X = reshape_X.transpose(1, 0)
        state = self.transformer(reshape_X)
        state = state.transpose(1, 0)
        state = self.state_ln(self.activation(state[:,:1,:]))
        var_idx = self.var_idx_embed(var_idx)
        orig_var_idx = var_idx
        var_idx = self.var_idx_ff(var_idx)
        var_idx = self.var_idx_ln(var_idx + orig_var_idx).unsqueeze(1)
        #var_idx = var_idx.expand(X.shape[0], 1, self.st_var)
        #state = state.squeeze(1).unsqueeze(0)
        #state = state.transpose(1, 0)
        #state = self.state_mha(var_idx.transpose(1, 0), state, state)[0]
        #state = state.transpose(1, 0)
        #state = state.squeeze(0).unsqueeze(1)
        #state = self.state_ln(state + var_idx)
        #state = self.state_ff(state)
        state = self.state_ff(torch.cat((var_idx, state), 2))
        rule_len = torch.unsqueeze(rule_len, 1)
        rule_len = self.rule_len_embed(rule_len)
        orig_rule_len = rule_len
        rule_len = self.rule_ff(rule_len)
        rule_len = self.rule_ln_2(rule_len + orig_rule_len)
        #inp = torch.cat((rule_len, state), 2)
        #state = state.squeeze(1).unsqueeze(0)
        #state = state.transpose(1, 0)
        #inp = self.rule_mha(rule_len.transpose(1, 0), state, state)
        #inp = inp[0].squeeze(0).unsqueeze(1)
        #inp = inp[0].transpose(1, 0)
        inp = self.rule_mha(rule_len, state)
        inp = self.rule_ln(inp + rule_len)
        old_inp = inp
        inp = self.prog_ff(inp)
        inp = self.prog_ln(inp + old_inp)
        return self.prog_pred[rule_len](inp)


class NoamOpt(object):
    def __init__(self, model_size, warmup, optimizer):
        self.optimizer = optimizer
        self._step = 0
        self.warmup = warmup
        self.model_size = model_size
        self._rate = 0

    def state_dict(self):
        return {k: v for k, v in self.__dict__.items() if k != 'optimizer'}

    def load_state_dict(self, state_dict):
        self.__dict__.update(state_dict)

    def step(self):
        self._step += 1
        rate = self.rate()
        for p in self.optimizer.param_groups:
            p['lr'] = rate
        self._rate = rate
        self.optimizer.step()

    def rate(self, step=None):
        if step is None:
            step = self._step
        return (self.model_size ** (-0.5) * min(step ** (-0.5), step * self.warmup ** (-1.5)))


class TransitionDataset(Dataset):
    def __init__(self, expected_size):
        self.conn = sqlite3.connect(dataset_name)
        self.conn.execute('PRAGMA CACHE_SIZE = 10000')
        self.conn.execute('PRAGMA threads = 2')
        self.conn.execute(f'PRAGMA mmap_size = {2*1024*1024*1024}')
        self.conn.execute('PRAGMA temp_store = 2')
        self.expected_size = expected_size
        self.cur = self.conn.cursor()

    def __len__(self):
        return self.expected_size
        #self.cur.execute('SELECT COUNT(*) FROM logic_programs')
        #row = self.cur.fetchone()
        #return min(row[0], self.expected_size)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            assert False
            idx = idx.tolist()

        self.cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (idx+1,))
        prog = self.cur.fetchone()
        self.cur.execute(f'SELECT state1, state2 FROM transitions WHERE prog_id = ? LIMIT {int(2**num_vars * 0.8)}', (idx+1,))
        data = self.cur.fetchall()
        #np.random.shuffle(data)
        if not prog:
            assert False, f'{idx}: {len(data)}'
        return (torch.tensor(data), torch.tensor(pickle.loads(prog[0]), dtype=torch.float))


def generate_single_datapoint():
    prog, m = generate_program(num_vars, True)
    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 5,
    }
    local = {}
    exec(func_str, globals_scope, local)
    state_transitions = {}
    for starting_state in np.random.permutation(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        new_transitions = []
        for t in transitions:
            state = convert_state_to_numbers([1 if x else 0 for x in t])
            new_transitions.append(state)
        for i in range(len(new_transitions)-1):
            if new_transitions[i] not in state_transitions:
                state_transitions[new_transitions[i]] = new_transitions[i+1]
            else:
                assert state_transitions[new_transitions[i]] == new_transitions[i+1]
        if len(state_transitions.keys()) == 2 ** num_vars:
            break
    p = [(i, []) for i in range(num_vars)]
    for e_i, e_j in state_transitions.items():
        for a in range(num_vars):
            if e_j & (1 << a) == 0:
                rule = (a, [
                    (0 if e_i & (1 << b) != 0 else 1,
                     (b, 1))
                    for b in range(num_vars)
                ])
                conflicts = []
                for r in p:
                    if r[0] == rule[0] and subsumes(r[1], rule[1]):
                        conflicts.append(r)
                for r in conflicts:
                    p.remove(r)
                for r in conflicts:
                    for l in rule[1]:
                        pos_l = (l[0], (l[1][0], 1))
                        neg_l = (0 if l[0] else 1, (l[1][0], 1))
                        if pos_l not in r[1] and neg_l not in r[1]:
                            r_c = (r[0], [(x[0], (x[1][0], 1)) for x in r[1]])
                            r_c[1].append(neg_l)
                            if not prog_subsumes(p, r_c):
                                p = [
                                    r_p for r_p in p
                                    if (
                                            r_c[0] == r_p[0] and not subsumes(r_c[1], r_p[1])
                                    ) or r_c[0] != r_p[0]
                                ]
                                p.append(r_c)
    one_hot_prog = convert_program_to_one_hot(p, num_vars)
    one_hot_prog = one_hot_prog.reshape(-1)
    prog_hash = hashlib.md5((':'.join([str(r) for r in one_hot_prog])).encode('utf-8')).digest()
    return state_transitions, one_hot_prog, prog_hash

'''
conn = sqlite3.connect(dataset_name)
cur = conn.cursor()
cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (2,))
prog = cur.fetchone()
prog = pickle.loads(prog[0])
prog = [index_to_prog(j, num_vars) for j, r in enumerate(prog) if r > 0]
print(human_program(prog, '', num_vars, GENERATE_READABLE))
import sys
sys.exit(0)
'''

def generate_dataset(seed, queue, can_stop):
    np.random.seed(seed)
    while not can_stop.is_set():
        queue.put_nowait(generate_single_datapoint())


def generate():
    if has_s3:
        import boto3
        s3 = boto3.resource('s3')

    conn = sqlite3.connect(dataset_name)
    conn.execute('PRAGMA journal_mode = \'WAL\'')
    conn.execute('PRAGMA temp_store = 2')
    conn.execute('PRAGMA synchronous = 1')
    conn.execute('PRAGMA cache_size = -64000')
    '''
    conn.execute('DROP TABLE IF EXISTS transitions')
    conn.execute('DROP TABLE IF EXISTS logic_programs')
    conn.execute('CREATE TABLE transitions (id INTEGER PRIMARY KEY AUTOINCREMENT, state1 INTEGER, state2 INTEGER, prog_id INTEGER)')
    conn.execute('CREATE TABLE logic_programs (id INTEGER PRIMARY KEY AUTOINCREMENT, hash TEXT UNIQUE, one_hot_prog BLOB)')
    conn.execute('CREATE INDEX prog_id_idx ON transitions (prog_id)')
    '''
    conn.commit()
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM logic_programs')
    row = cur.fetchone()
    generated = row[0]

    with Manager() as manager:
        can_stop = Event()
        queue = manager.Queue()
        processes = []
        for j in range(os.cpu_count() - 2):
            seed = int(j + time.time() % 10000)
            p = Process(target=generate_dataset, args=(seed, queue, can_stop))
            p.start()
            processes.append(p)

        pbar = tqdm(total=train_size + val_size + test_size, initial=generated)
        errors = 0
        while generated < train_size + val_size + test_size:
            state_transitions, one_hot_prog, prog_hash = queue.get()
            pbar.set_postfix(size=queue.qsize(), errors=errors)
            try:
                cur = conn.cursor()
                cur.execute('INSERT INTO logic_programs (hash, one_hot_prog) VALUES (?, ?)', (prog_hash, pickle.dumps(one_hot_prog)))
                conn.executemany(f'INSERT INTO transitions (state1, state2, prog_id) VALUES (?, ?, {cur.lastrowid})', state_transitions.items())
                conn.commit()
                generated += 1
                pbar.update(1)
                if generated % 5000 == 0 and has_s3:
                    s3.Object('phua-tmp', 'model5_dataset_004.db').upload_file(dataset_name)
            except Exception:
                errors += 1
                pbar.set_postfix(size=queue.qsize(), errors=errors)

        can_stop.set()
        for p in processes:
            p.join()

        pbar.close()
    print('Done')


def to_tuple(lst):
    return tuple(to_tuple(i) if isinstance(i, list) else i for i in lst)

'''
len_pos_weights = {}
for i in range(num_vars+1):
    len_pos_weights[i] = torch.zeros(max_rule_len, dtype=torch.float)
    if i > 0:
        curr_len = length(i, num_vars) - length(i-1, num_vars)
    else:
        curr_len = 1
    len_pos_weights[i][:curr_len] = 1.
    len_pos_weights[i] = len_pos_weights[i].to(device)
'''


from data import index_to_prog


if do_params:
    from matplotlib import pyplot as plt
    x = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
    y = [12281612,12294488,12348880,12428896,12804144,13339296,16016960,19764800,39316096,66362944,211390848,410512320,1498340096,2984410688,11210531968,22408794304]
    plt.plot(x, y, label='With Sharing')
    y = [12289307,12323729,12432499,12720793,13638795,16130529,24045923,45920105,115024123,308655409,916599123,2638816569,8014233707]
    plt.plot(x[:-3], y, label='Without Sharing')
    plt.xlabel('Number of Variables')
    plt.ylabel('Parameters')
    plt.legend()
    plt.savefig('params.png')
    import sys
    sys.exit(0)
    model = SetTransformer(tuple_length, 1, max_rule_len, dim_hidden=512, num_heads=8, ln=True)
    pytorch_total_params = sum(p.numel() for p in model.parameters())
    print(pytorch_total_params)


idx_to_rule_dict = {}
for i in range(length(num_vars, num_vars)):
    rules = []
    b = index_to_rule(i)
    for a in b:
        rules.append(to_tuple(a))
    idx_to_rule_dict[i] = rules


idxes = [(0, 1)]
for i in range(1, num_vars+1):
    idxes.append((length(i-1, num_vars), length(i, num_vars)))

rule_lengths = [1]
for i in range(1, num_vars+1):
    rule_lengths.append(length(i, num_vars))

max_tensor = torch.ones(max_rule_len, device=device)*0.7


def get_batch(data, rule_len, var_idx, is_evaluate=False):
    batch_size = data[0].shape[0]
    var_idx_in = torch.tensor(var_idx)
    rules_idx = torch.stack([torch.arange(a * (3 ** num_vars) + rule_lengths[rule_len-1], a * (3 ** num_vars) + rule_lengths[rule_len], device=device) for a in var_idx])
    var_idx = torch.tensor([1 << a for a in var_idx])
    var_idx = var_idx.unsqueeze(1).expand(batch_size, data[0].shape[1])
    inp = data[0][:,:,0] + (2 ** num_vars) * ((data[0][:,:,1] & var_idx) > 0)
    target = data[1].to(device=device)
    rules_idxes = torch.gather(target, 1, rules_idx)[:,:max_rule_len]
    targets = []
    start_idx = idxes[rule_len][0]
    end_idx = idxes[rule_len][1]
    for i in range(batch_size):
        rule_idxes = torch.nonzero(rules_idxes[i], as_tuple=True)[0]
        prog = torch.zeros(max_rule_len, dtype=torch.float, device=device)
        prog[rule_idxes] = 1
        #rules = [idx_to_rule_dict[r.item()] for r in torch.nonzero(data[1][i][var_idx_in[i].item() * (3 ** num_vars):(var_idx_in[i].item()+1) * (3 ** num_vars)], as_tuple=True)[0]]
        #rules = [r.item() for r in torch.nonzero(data[1][i][var_idx_in[i].item() * (3 ** num_vars):(var_idx_in[i].item()+1) * (3 ** num_vars)], as_tuple=True)[0]]
        #rules_t = torch.zeros(3 ** num_vars, dtype=torch.bool)
        #rules_t[np.array(rules)] = 1
        rules_t = data[1][i][var_idx_in[i].item() * (3 ** num_vars):(var_idx_in[i].item()+1) * (3 ** num_vars)].to(dtype=torch.bool)
        #rules = [r for r in rules if len(r) == rule_len-1]
        t = 0
        if not is_evaluate and len(rule_idxes) == 0:
            subs = torch.zeros(max_rule_len, device=device)
            subs2 = subsumption_mat[:,start_idx:end_idx].clone()
            subs2[~rules_t,:] = 0
            subs[:end_idx-start_idx] = torch.sum(subs2, dim=0)
            subs3 = torch.zeros(max_rule_len, device=device)
            subs2 = subsumption_mat[start_idx:end_idx,:].clone()
            subs2[:,~rules_t] = 0
            subs3[:end_idx-start_idx] = torch.sum(subs2, dim=1)
            subs += subs3
            subs[prog.to(dtype=torch.bool)] = 0
            subs = torch.min(subs*0.3, max_tensor)
            prog += subs
        targets.append(prog)
    targets = torch.stack(targets)
    #total_zeros += len((torch.nonzero(targets, as_tuple=True)[1] == 0).nonzero(as_tuple=True)[0])
    #print(rule_len, (torch.nonzero(targets, as_tuple=True)[1] == 0).nonzero(as_tuple=True)[0])
    #rule_lens = F.one_hot(torch.tensor(rule_len), num_vars+1)
    #rule_lens = rule_lens.unsqueeze(0).expand(batch_size, num_vars+1)
    rule_lens = torch.tensor(rule_len).unsqueeze(0).expand(batch_size)
    return inp.to(device), targets.to(device), rule_len, rule_lens.to(device), var_idx_in.to(device)

if do_generate:
    generate()

if do_stats:
    conn = sqlite3.connect(dataset_name)
    conn.execute('PRAGMA CACHE_SIZE = 1000000')
    #conn.execute('PRAGMA threads = 2')
    #conn.execute(f'PRAGMA mmap_size = {2*1024*1024*1024}')
    conn.execute('PRAGMA temp_store = MEMORY')
    cur = conn.cursor()
    #stats = {}
    #count = {}
    stats = torch.zeros(max_rule_len, device=device)
    count = 0
    #for rule_len in range(1, 5):
    #    stats[rule_len] = [torch.zeros(max_rule_len) for _ in range(num_vars)]
    #    count[rule_len] = [0 for _ in range(num_vars)]
    pbar = tqdm(total=train_size + val_size + test_size)
    try:
        for prog in cur.execute('SELECT one_hot_prog FROM logic_programs'):
            prog = torch.tensor(pickle.loads(prog[0]), dtype=torch.float)
            prog = prog.unsqueeze(0).expand(num_vars, prog.shape[0])
            for rule_len in range(1, num_vars+1):
                rules_idx = torch.stack([torch.arange(a * (3 ** num_vars) + rule_lengths[rule_len-1], a * (3 ** num_vars) + rule_lengths[rule_len]) for a in range(num_vars)])
                rules_idxes = torch.gather(prog, 1, rules_idx)[:,:max_rule_len]
                for var_idx in range(num_vars):
                    rule_idxes = torch.nonzero(rules_idxes[var_idx], as_tuple=True)[0]
                    #stats[rule_len][var_idx][rule_idxes] += 1
                    #count[rule_len][var_idx] += 1
                    stats[rule_idxes] += 1
                    count += 1
            pbar.update(1)
    except KeyboardInterrupt:
        pass
    pbar.close()
    conn.close()

    '''
    for rule_len in range(1, 5):
        for var_idx in range(num_vars):
            count[rule_len][var_idx] = torch.full(stats[rule_len][var_idx].shape, count[rule_len][var_idx], dtype=torch.long)
            neg = count[rule_len][var_idx] - stats[rule_len][var_idx]
            stats[rule_len][var_idx][stats[rule_len][var_idx] == 0] = 1
            pos_weights = neg / stats[rule_len][var_idx]
            print(pos_weights)
            torch.save(pos_weights, f'pos_weights_5_{rule_len}_{var_idx}.pt')
    '''
    count = torch.full(stats.shape, count, dtype=torch.long, device=device)
    neg = count - stats
    stats[stats == 0] = 1
    pos_weights = neg / stats
    torch.save(pos_weights, 'pos_weights_7.pt')
    '''
    from matplotlib import pyplot as plt
    stats = stats.cpu().numpy()
    fig, ax = plt.subplots(figsize =(10, 7))
    ax.bar(range(max_rule_len), stats)
    plt.savefig('stats.png')
    '''

rule_len_masks = {}
for rule_len in range(1, num_vars+1):
    mask = torch.zeros(batch_size, max_rule_len, device=device, dtype=torch.bool)
    mask[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])] = 1
    rule_len_masks[rule_len] = mask

if do_train:
    if os.path.isfile(f'subsumption_mat_{num_vars}.pt'):
        subsumption_mat = torch.load(f'subsumption_mat_{num_vars}.pt')
    else:
        subsumption_mat = torch.zeros(3 ** num_vars, 3 ** num_vars, device=device)
        for i in range(3 ** num_vars):
            idx_i = idx_to_rule_dict[i]
            for j in range(3 ** num_vars):
                idx_j = idx_to_rule_dict[j]
                subsumption_mat[i][j] = 1 if subsumes(idx_i, idx_j) else 0
        torch.save(subsumption_mat, f'subsumption_mat_{num_vars}.pt')

    print('Initializing data set...')
    dataset = TransitionDataset(train_size + val_size + test_size)

    train_data, val_data, test_data = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
    train_dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=36)
    val_dataloader = DataLoader(val_data, batch_size=eval_batch_size, shuffle=True, num_workers=8)
    test_dataloader = DataLoader(test_data, batch_size=eval_batch_size, num_workers=4)

    print('Initializing model...')
    model = SetTransformer(tuple_length, 1, max_rule_len, dim_hidden=512, num_heads=8, ln=True)
    #model = torch.load('model7')
    #optimizer = torch.optim.Adam(model.parameters(), lr=1e-4, weight_decay=1e-2)
    optimizer = torch.optim.Adadelta(model.parameters(), lr=1e-3, weight_decay=1e-6)
    #optimizer = torch.optim.Adadelta(model.parameters(), lr=1e-3, weight_decay=1e-6)
    criterions = {}
    pos_weights = torch.load('pos_weights_7.pt').to(device)
    train_criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weights)
    #train_criterion = nn.BCEWithLogitsLoss()
    train_criterion_2 = nn.MSELoss()
    val_criterion = nn.BCEWithLogitsLoss()
    '''
    for rule_len in range(1, 5):
        pos_weights = []
        for i in range(num_vars):
            weights = torch.load(f'pos_weights_5_{rule_len}_{i}.pt')
            pos_weights.append(weights)
        pos_weights = torch.sum(torch.stack(pos_weights), dim=0).to(device)
        criterions[rule_len] = nn.BCEWithLogitsLoss(pos_weight=pos_weights[:280])
    '''
    model = model.to(device)

    try:
        count = 0
        for epoch in range(train_epochs):
            model.train()
            losses = []
            running_loss = 0.0
            pbar = tqdm(total=train_size / batch_size)
            for batch, batched_data in enumerate(train_dataloader):
                for rule_len in np.random.permutation(range(1, num_vars+1))[:1]:#np.random.randint(1, 4, size=3):
                    var_idx = np.random.randint(num_vars, size=batch_size)
                    mask = rule_len_masks[rule_len]
                    data, targets, rule_len, rule_lens, var_idx = get_batch(batched_data, rule_len, var_idx)
                    preds = model(data, rule_lens, var_idx)
                    #print(preds.shape)
                    preds = preds.reshape(preds.shape[0], -1)
                    preds[~mask] = 0
                    #preds = torch.mul(preds, mask)
                    #loss = criterions[rule_len](preds, targets)
                    loss = train_criterion(preds, targets) + train_criterion_2(torch.sigmoid(preds[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])]), targets[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])])

                    optimizer.zero_grad()
                    loss.backward()
                    #nn.utils.clip_grad_norm_(model.parameters(), max_norm=5.0, norm_type=2)
                    #nn.utils.clip_grad_value_(model.parameters(), clip_value=1.5)
                    optimizer.step()

                    losses.append(loss.item())
                    running_loss += loss.item()
                if batch % 50 == 49:
                    total_norm = 0
                    for p in model.parameters():
                        if p.grad is not None:
                            param_norm = p.grad.detach().data.norm(2)
                            total_norm += param_norm.item() ** 2
                    total_norm = total_norm ** 0.5
                    writer.add_scalar('grad norm', total_norm, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('training loss', running_loss / 100, epoch * (train_size / batch_size) + batch)
                    for i in [1, 4]:
                        writer.add_histogram(f'prog_pred/{i}.bias', model.prog_pred[i].bias, epoch * (train_size / batch_size) + batch)
                        writer.add_histogram(f'prog_pred/{i}.weight', model.prog_pred[i].weight, epoch * (train_size / batch_size) + batch)
                        writer.add_histogram(f'prog_pred/{i}.weight.grad', model.prog_pred[i].weight.grad, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'state_embed_ff/bias', model.state_embed_ff.bias, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'state_embed_ff/weight', model.state_embed_ff.weight, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'state_embed_ff/weight.grad', model.state_embed_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'rule_ff/bias', model.rule_ff.bias, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'rule_ff/weight', model.rule_ff.weight, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'rule_ff/weight.grad', model.rule_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'var_idx_ff/bias', model.var_idx_ff.bias, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'var_idx_ff/weight', model.var_idx_ff.weight, epoch * (train_size / batch_size) + batch)
                    writer.add_histogram(f'var_idx_ff/weight.grad', model.var_idx_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    running_loss = 0.0
                pbar.update(1)
                pbar.set_postfix(loss=loss.item())
            pbar.close()

            avg_loss = np.mean(losses)
            print(f'Epoch {epoch}: train loss {avg_loss:.3f}')

            model.eval()
            losses = []
            for batched_data in test_dataloader:
                #rule_len = np.random.randint(num_vars+1)
                rule_len = np.random.randint(3) + 1
                var_idx = np.random.randint(num_vars, size=eval_batch_size)
                data, targets, rule_len, rule_lens, var_idx = get_batch(batched_data, rule_len, var_idx, True)
                preds = model(data, rule_lens, var_idx)
                preds = preds.reshape(preds.shape[0], -1)
                loss = val_criterion(preds, targets)
                losses.append(loss.item())

            avg_loss = np.mean(losses)
            writer.add_scalar('test loss', avg_loss, epoch)
            print(f'Epoch {epoch}: test loss {avg_loss:.3f}')
            #break
    except KeyboardInterrupt:
        pass

    '''
    count = torch.full(stats.shape, count, dtype=torch.long, device=device)
    neg = count - stats
    stats[stats == 0] = 1
    pos_weights = neg / stats
    print(pos_weights)
    #torch.save(pos_weights, 'pos_weights_5.pt')
    from matplotlib import pyplot as plt
    stats = stats.cpu().numpy()
    fig, ax = plt.subplots(figsize =(10, 7))
    ax.bar(range(max_rule_len), stats)
    plt.savefig('stats.png')
    '''
    torch.save(model, 'model7')

if do_test:
    model = torch.load('model7')
    model.eval()

    if num_vars == 3:
        prog = [
            (0, [(1, (0, 1)), (1, (1, 1))]),
            (0, [(1, (0, 1)), (1, (2, 1))]),
            (1, [(1, (0, 1)), (0, (1, 1))]),
            (2, [(1, (0, 1))]),
            (2, [(0, (1, 1))]),
        ]
        '''
        prog = [
            (0, [(0, (0, 1)), (0, (1, 1))]),
            (0, [(0, (1, 1)), (0, (2, 1))]),
            (1, [(0, (0, 1))]),
            (1, [(0, (1, 1)), (0, (2, 1))]),
            (2, [(1, (0, 1))]),
            (2, [(1, (2, 1))]),
        ]
        prog = [
            (0, [(0, (0, 1)), (0, (1, 1))]),
            (0, [(0, (1, 1)), (0, (2, 1))]),
            (1, [(0, (0, 1))]),
            (1, [(0, (1, 1)), (0, (2, 1))]),
            (2, [(1, (0, 1))]),
            (2, [(1, (2, 1))]),
        ]
        '''

    if num_vars == 5:
        prog = [
            (0, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
            (0, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (1, [(0, (2, 1)), (1, (2, 1)), (1, (3, 1))]),
            (1, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (2, [(0, (0, 1)), (0, (1, 1)), (0, (3, 1))]),
            (2, [(0, (0, 1)), (1, (1, 1)), (1, (3, 1))]),
            (3, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
            (3, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (3, [(1, (1, 1)), (0, (3, 1))]),
            (4, [(1, (0, 1)), (0, (1, 1)), (0, (2, 1))]),
        ]
        '''
        conn = sqlite3.connect(dataset_name)
        cur = conn.cursor()
        cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (7,))
        prog = cur.fetchone()
        prog = pickle.loads(prog[0])
        prog = [index_to_prog(j, num_vars) for j, r in enumerate(prog) if r > 0]
        '''

    if num_vars == 7:
        prog = [
            (0, [(1, (3, 1)), (1, (2, 1))]),
            (0, [(1, (3, 1)), (0, (4, 1))]),
            (0, [(0, (3, 1)), (0, (2, 1)), (1, (4, 1))]),
            (1, [(0, (0, 1)), (1, (2, 1))]),
            (1, [(0, (1, 1)), (1, (2, 1))]),
            (1, [(0, (1, 1)), (1, (0, 1))]),
            (2, [(1, (4, 1)), (1, (0, 1))]),
            (2, [(0, (5, 1)), (0, (0, 1))]),
            (2, [(0, (4, 1)), (0, (0, 1))]),
            (3, [(1, (2, 1)), (0, (1, 1))]),
            (3, [(0, (4, 1)), (1, (1, 1))]),
            (3, [(0, (2, 1)), (1, (1, 1))]),
            (4, [(0, (3, 1)), (1, (2, 1))]),
            (4, [(1, (0, 1)), (0, (3, 1))]),
            (4, [(0, (0, 1)), (1, (2, 1))]),
            (5, [(1, (5, 1)), (0, (0, 1))]),
            (5, [(1, (6, 1)), (0, (5, 1)), (1, (0, 1))]),
            (5, [(0, (6, 1)), (0, (0, 1))]),
            (6, [(1, (4, 1)), (0, (2, 1)), (0, (6, 1))]),
            (6, [(0, (4, 1)), (1, (2, 1)), (0, (6, 1))]),
        ]
        prog = [
            (3, [(0, (3, 1))]),
            (1, [(0, (5, 1)), (0, (3, 1))]),
            (1, [(0, (5, 1)), (0, (1, 1))]),
            (1, [(0, (3, 1)), (0, (2, 1))]),
            (2, [(1, (6, 1))]),
            (5, [(0, (3, 1))]),
            (5, [(0, (2, 1))]),
            (6, [(0, (6, 1))]),
            (6, [(1, (1, 1))]),
            (0, [(1, (5, 1))]),
            (4, [(1, (6, 1))]),
            (4, [(0, (1, 1))]),
        ]

    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 2,
    }
    local = {}
    exec(func_str, globals_scope, local)
    data = []
    for starting_state in np.random.permutation(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([convert_state_to_numbers([1 if x else 0 for x in transitions[0]]), convert_state_to_numbers([1 if x else 0 for x in transitions[1]])])
    data = [data]
    #print(data)

    #print(3 ** num_vars)
    data = torch.tensor(data)
    total_pred = []
    for i in range(num_vars):
        print(f'====================\nVar {i}\n===========================')
        var_idx = torch.full((data.shape[0], data.shape[1]), 1 << i, dtype=torch.long)
        var_idx_in = torch.full((num_vars+1,), i, dtype=torch.long, device=device)
        in_data = torch.clone(data)
        in_data[:,:,1] = (in_data[:,:,1] & var_idx) > 0
        inp = in_data[:,:,0] + (2 ** num_vars) * in_data[:,:,1]
        in_data = inp.to(device)
        #in_data = in_data.to(device)
        rule_len = torch.arange(num_vars+1).to(device, dtype=torch.long)
        in_data = in_data.expand(num_vars+1, 2 ** num_vars)
        preds_out = model(in_data, rule_len, var_idx_in)
        for rule_len in range(1, num_vars+1):
            preds_out[rule_len][~rule_len_masks[rule_len][:1,:]] = 0
        print(torch.topk(torch.sigmoid(preds_out), 5, dim=2).indices)
        preds = [torch.tensor([-999], device=device)]
        #for i in range(0, num_vars+1):
        for i in range(1, 5):
        #for i in range(1, 4):
            if i > 0:
                t = preds_out[i,0,:length(i, num_vars)-length(i-1, num_vars)]
            else:
                t = preds_out[i,0,:1]
            preds.append(t)
        preds.append(torch.full((length(num_vars, num_vars) - length(4, num_vars),), -999, dtype=torch.float).to(device))
        preds = torch.cat(preds)
        print(torch.sigmoid(preds))
        print(torch.topk(preds, 10))
        total_pred.append(preds)
    output = torch.stack(total_pred)
    total_pred = torch.cat(total_pred)
    preds = total_pred.unsqueeze(0)
    #print(torch.sigmoid(preds[0]).tolist()[6*(3**num_vars):])
    targets = torch.tensor([convert_program_to_one_hot(prog, num_vars)], dtype=torch.float).to(device)
    targets = targets.reshape(1, -1)
    criterion = nn.BCEWithLogitsLoss()
    val = criterion(preds, targets)
    print(val.item())
    #print(output_idx)
    print(preds.shape)
    print(torch.topk(torch.sigmoid(output), num_vars*2, dim=1))
    output_idx = (torch.sigmoid(output) >= 0.9).bool()
    output[~output_idx] = 0
    #output[output_idx] = 1
    output_2 = torch.topk(output, num_vars*2, dim=1)
    print(output_2)
    output_idx = output_2.indices
    print(output_idx)
    output = torch.zeros_like(output, device='cpu')
    for i, idxes in enumerate(output_idx.tolist()):
        output[i][idxes] = 1
    print(output.shape)
    output = output.reshape(-1)
    pred_prog_rules = [(i, []) for i in range(num_vars)]
    l = list(enumerate(output))
    for i, r in l:
        if r:
            new_r = index_to_prog(i, num_vars)
            new_r[1] = to_tuple(new_r[1])
            conflicts = []
            for a in pred_prog_rules:
                if new_r[0] == a[0] and subsumes(a[1], new_r[1]):
                    conflicts.append(a)
                    pred_prog_rules.remove(a)
            if len(conflicts) == 0:
                pred_prog_rules.append(new_r)
            for a in conflicts:
                for l in new_r[1]:
                    pos_l = (l[0], (l[1][0], 1))
                    neg_l = (0 if l[0] else 1, (l[1][0], 1))
                    if pos_l not in a[1] and neg_l not in a[1]:
                        r_c = (a[0], [(x[0], (x[1][0], 1)) for x in a[1]])
                        r_c[1].append(neg_l)
                        if not prog_subsumes(pred_prog_rules, r_c):
                            pred_prog_rules = [
                                r_p for r_p in pred_prog_rules
                                if (
                                        r_c[0] == r_p[0] and not subsumes(r_c[1], r_p[1])
                                ) or r_c[0] != r_p[0]
                            ]
                            pred_prog_rules.append(r_c)
    print('Predicted:')
    print(human_program(pred_prog_rules, '', num_vars, GENERATE_READABLE))
    print('Truth:')
    print(human_program(prog, '', num_vars, GENERATE_READABLE))

    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 2,
    }
    local = {}
    exec(func_str, globals_scope, local)
    data = []
    for starting_state in range(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
    truth_data = np.array(data)

    func_str = human_program(pred_prog_rules, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 2,
    }
    local = {}
    exec(func_str, globals_scope, local)
    data = []
    for starting_state in range(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
    pred_data = np.array(data)
    print(truth_data.tolist())
    print(pred_data.tolist())
    print((np.square(pred_data - truth_data).mean()))
