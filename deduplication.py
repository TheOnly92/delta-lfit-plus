import pickle
from tqdm import tqdm


with open('training_data.pickle', 'rb') as f:
    dataset, _ = pickle.load(f)

seen_transitions = set()
for data in tqdm(dataset):
    s = hash(':'.join([str(d) for d in data[:12]]))
    if s not in seen_transitions:
        seen_transitions.add(s)

print(f'Total duplicate transitions: {len(dataset)-len(seen_transitions)}')
