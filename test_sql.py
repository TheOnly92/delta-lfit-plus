import pickle
import sqlite3
from tqdm import tqdm

conn = sqlite3.connect('model5_dataset_001.db')
cur = conn.cursor()

cur.execute('SELECT COUNT(*) FROM logic_programs')
row = cur.fetchone()
count = 0
pbar = tqdm(total=row[0])
try:
    for row in cur.execute('SELECT one_hot_prog FROM logic_programs'):
        prog = pickle.loads(row[0])
        has_empty = False
        for i in range(7):
            if prog[i*(3**7)]:
                has_empty = True
                break
        if not has_empty:
            count += 1
        pbar.update(1)
except KeyboardInterrupt:
    pass
pbar.close()
print(count)
