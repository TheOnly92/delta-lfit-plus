import argparse
import datetime as dt
import math
import pickle
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import sqlite3
import logging
import numpy as np
import hashlib
import os
from tqdm import tqdm
from multiprocessing import Event, Process, Manager
import redis
from data import (
    length,
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    GENERATE_READABLE,
    convert_program_to_one_hot,
    subsumes,
    prog_subsumes,
    index_to_prog,
    index_to_rule,
)
from torch.utils.tensorboard import SummaryWriter


num_vars = 3
delays = 1
dataset_name = 'model9_dataset_001.db'
#train_size = int(5000*1024)
val_size = 1024
test_size = 1024
generate_only = False
do_generate = False
generate_reset = True
do_stats = False
do_params = False
do_train = True
do_test = False
has_s3 = False
train_size = int(((2 ** (2 ** (num_vars * delays))) ** num_vars)*0.7) // 1024 * 1024

tuple_length = 2
train_epochs = 100
batch_size = 64
eval_batch_size = 16

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('model9')
logger.addHandler(logging.FileHandler(f'{time.strftime("%Y%m%d-%H%M")}.log', mode='w'))
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

#half_point = math.ceil(num_vars/2)
#max_rule_len = length(half_point, num_vars) - length(half_point-1, num_vars)
#max_rule_len = length(5, num_vars) - length(4, num_vars)
max_rule_len = 0
max_rule_len_x = 0
for i in range(1, num_vars*delays):
    if length(i, num_vars*delays) - length(i-1, num_vars*delays) > max_rule_len:
        max_rule_len_x = i
    max_rule_len = max(max_rule_len, length(i, num_vars*delays) - length(i-1, num_vars*delays))

writer = SummaryWriter(f'runs/exp_var_{num_vars}_{delays}_{dt.datetime.now():%Y%m%d%H%M%S}')


def convert_state_to_numbers(state):
    return sum([s * (2**i) for i, s in enumerate(state)])


class MAB(nn.Module):
    def __init__(self, dim_Q, dim_K, dim_V, num_heads, ln=False):
        super(MAB, self).__init__()
        self.dim_V = dim_V
        self.num_heads = num_heads
        self.fc_q = nn.Linear(dim_Q, dim_V)
        self.fc_k = nn.Linear(dim_K, dim_V)
        self.fc_v = nn.Linear(dim_K, dim_V)
        self.ln0 = None
        self.ln1 = None
        if ln:
            self.ln0 = nn.LayerNorm(dim_V)
            self.ln1 = nn.LayerNorm(dim_V)
        self.fc_o = nn.Linear(dim_V, dim_V)

    def forward(self, Q, K):
        Q = self.fc_q(Q)
        K, V = self.fc_k(K), self.fc_v(K)

        dim_split = self.dim_V // self.num_heads
        Q_ = torch.cat(Q.split(dim_split, 2), 0)
        K_ = torch.cat(K.split(dim_split, 2), 0)
        V_ = torch.cat(V.split(dim_split, 2), 0)

        A = torch.softmax(Q_.bmm(K_.transpose(1, 2)) / math.sqrt(self.dim_V), 2)
        O = torch.cat((Q_ + A.bmm(V_)).split(Q.size(0), 0), 2)
        O = O if self.ln0 is None else self.ln0(O)
        O = O + F.relu(self.fc_o(O))
        O = O if self.ln1 is None else self.ln1(O)
        return O


class SAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, ln=False):
        super(SAB, self).__init__()
        self.mab = MAB(dim_in, dim_in, dim_out, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(X, X)


class ISAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, num_inds, ln=False):
        super(ISAB, self).__init__()
        self.I = nn.Parameter(torch.Tensor(1, num_inds, dim_out))
        nn.init.xavier_uniform_(self.I)
        self.mab0 = MAB(dim_out, dim_in, dim_out, num_heads, ln=ln)
        self.mab1 = MAB(dim_in, dim_out, dim_out, num_heads, ln=ln)

    def forward(self, X):
        H = self.mab0(self.I.repeat(X.size(0), 1, 1), X)
        return self.mab1(X, H)


class PMA(nn.Module):
    def __init__(self, dim, num_heads, num_seeds, ln=False):
        super(PMA, self).__init__()
        self.S = nn.Parameter(torch.Tensor(1, num_seeds, dim))
        nn.init.xavier_uniform_(self.S)
        self.mab = MAB(dim, dim, dim, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(self.S.repeat(X.size(0), 1, 1), X)


class InputEmbeddings(nn.Module):
    def __init__(self, dim_in, dim_out):
        super(InputEmbeddings, self).__init__()
        self.activation = nn.ReLU()
        self.ln = nn.LayerNorm(dim_out)
        self.embed = nn.Embedding(dim_in, dim_out)

    def forward(self, X):
        return self.ln(self.activation(self.embed(X)))


class FFResidual(nn.Module):
    def __init__(self, dim_in, dim_hidden):
        super(FFResidual, self).__init__()
        self.activation = nn.ReLU()
        self.ff = nn.Sequential(
            nn.Linear(dim_in, dim_hidden),
            #nn.LayerNorm(dim_hidden),
            nn.GELU(),
            #nn.Dropout(0.2),
            #self.activation,
            nn.Linear(dim_hidden, dim_hidden),
            #nn.LayerNorm(dim_hidden),
            nn.GELU(),
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden, dim_hidden),
            #nn.LayerNorm(dim_hidden),
            nn.GELU(),
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden, dim_hidden),
            #nn.LayerNorm(dim_hidden),
            nn.GELU(),
            #nn.Dropout(0.2),
            #self.activation,
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden, dim_in),
        )
        self.ln = nn.LayerNorm(dim_in)

    def forward(self, X):
        return self.ln(self.ff(X) + X)


class SetTransformer(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output, num_inds=32, dim_hidden=128, num_heads=4, ln=False):
        super(SetTransformer, self).__init__()
        dim_hidden = 1024
        self.st_dim = 512
        self.st_var = 512
        st_dim_hidden = 1024
        var_idx_dim = 512
        self.enc = nn.Sequential(
            #ISAB(self.st_dim, st_dim_hidden, num_heads, num_inds, ln=ln),
            SAB(self.st_dim, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            #SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            #ISAB(st_dim_hidden, st_dim_hidden, num_heads, num_inds, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #SAB(32, dim_hidden, num_heads, ln=ln),
            #SAB(dim_hidden, dim_hidden, num_heads, ln=ln)
        )
        self.dec = nn.Sequential(
            PMA(st_dim_hidden, num_heads, num_outputs, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            SAB(st_dim_hidden, st_dim_hidden, num_heads, ln=ln),
            #nn.Dropout(0.2),
            nn.Linear(st_dim_hidden, self.st_var),
        )
        self.state_ln = nn.LayerNorm(self.st_var)
        self.activation = nn.ReLU()
        self.state_embed_ff = FFResidual(self.st_dim, dim_hidden)
        self.state_embed_ln = nn.LayerNorm(self.st_dim)
        self.state_embed_ff_2 = nn.Sequential(
            nn.Linear(self.st_dim, dim_hidden),
            self.activation,
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden, self.st_var + var_idx_dim),
        )
        self.state_embed_ff_2_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.var_idx_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_embed_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_ln = nn.LayerNorm(self.st_var)
        #self.state_mha = nn.MultiheadAttention(self.st_var, num_heads, dropout=0.2)
        #self.rule_len_embed = InputEmbeddings(max_rule_len*2, self.st_var + var_idx_dim)
        self.rule_len_embed = InputEmbeddings(max_rule_len*2, self.st_var)
        self.rule_ff = nn.Linear(self.st_var + var_idx_dim, self.st_var + var_idx_dim)
        self.state_embed = InputEmbeddings(2 ** (num_vars*delays) * 2, self.st_dim)
        self.var_idx_embed = InputEmbeddings(num_vars, var_idx_dim)
        self.var_idx_embed2 = InputEmbeddings(num_vars, var_idx_dim)
        self.var_idx_ff = FFResidual(var_idx_dim, dim_hidden)
        self.rule_len_ff = FFResidual(var_idx_dim, dim_hidden)
        #self.var_idx_ln = nn.LayerNorm(var_idx_dim)
        self.prog_ff = FFResidual(self.st_var + var_idx_dim, dim_hidden)
        self.state_mha = MAB(self.st_var, var_idx_dim, self.st_var + var_idx_dim, num_heads, ln=ln)
        #self.rule_mha = nn.MultiheadAttention(self.st_var + var_idx_dim, num_heads, dropout=0.1)
        self.rule_mha = MAB(self.st_var + var_idx_dim, self.st_var + var_idx_dim, self.st_var + var_idx_dim, num_heads, ln)
        self.rule_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.state_ff = FFResidual(self.st_var + var_idx_dim, dim_hidden)
        self.temp_ff = nn.Linear(self.st_var, self.st_var + var_idx_dim)
        self.rule_ln_2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.rule_ln_3 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.prog_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.inp_ln = nn.LayerNorm(self.st_var + var_idx_dim)
        self.inp_ln2 = nn.LayerNorm(self.st_var + var_idx_dim)
        self.var_idx_ff_embed = nn.Linear(var_idx_dim, self.st_var + var_idx_dim)
        self.prog_pred = nn.Sequential(
            nn.Linear(self.st_var + var_idx_dim, dim_hidden*2),
            nn.LayerNorm(dim_hidden*2),
            nn.Linear(dim_hidden*2, dim_hidden*2),
            nn.LayerNorm(dim_hidden*2),
            nn.Linear(dim_hidden*2, dim_hidden*2),
            nn.LayerNorm(dim_hidden*2),
            nn.Linear(dim_hidden*2, dim_hidden*2),
            nn.LayerNorm(dim_hidden*2),
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden*2, dim_output),
        )
        self.state_pred = nn.Sequential(
            nn.Linear(self.st_var + var_idx_dim + self.st_dim, dim_hidden),
            nn.LayerNorm(dim_hidden),
            #nn.Dropout(0.2),
            nn.Linear(dim_hidden, 1),
        )

    def forward(self, X, rule_len, var_idx, inp_state=None):
        reshape_X_orig = self.state_embed(X)
        var_idx_embed_orig = self.var_idx_ff(self.var_idx_embed(var_idx))
        var_idx_embed = F.normalize(var_idx_embed_orig).unsqueeze(1).expand((var_idx_embed_orig.shape[0], reshape_X_orig.shape[1], var_idx_embed_orig.shape[1]))
        rule_len = self.rule_len_ff(self.rule_len_embed(rule_len))
        rule_len_embed = F.normalize(rule_len).unsqueeze(1)
        reshape_X = self.state_embed_ff(reshape_X_orig) + var_idx_embed + rule_len_embed
        encoded = self.enc(reshape_X)
        state = self.state_ln(self.activation(self.dec(encoded)))
        #var_idx = self.var_idx_embed2(var_idx)
        #var_idx = var_idx_embed_orig
        #orig_var_idx = var_idx
        #var_idx = self.var_idx_ff(var_idx)
        #var_idx = self.var_idx_ln(var_idx)
        #var_idx = var_idx.unsqueeze(1)
        #state = self.state_ff(torch.cat((var_idx, state), 2))
        #state = self.state_mha(state, var_idx)
        state = self.temp_ff(state)
        #state = self.state_ff(state)
        inp = state
        #inp = self.rule_mha(rule_len, state)
        #rule_len = torch.unsqueeze(rule_len, 1)
        #inp = self.rule_mha(state, rule_len)
        #print(inp[1])
        #print(torch.var_mean(inp))
        #inp = self.inp_ln2(inp)
        #inp = self.rule_ln(inp + rule_len)
        inp = self.rule_ln(inp)# + self.var_idx_ff_embed(var_idx))
        #states_X = self.state_embed_ff_2(reshape_X_orig)
        #states_X = self.state_embed_ff_2_ln(states_X)
        #states_X = torch.max(states_X, 1, keepdim=True).values
        #states_X = torch.mean(states_X, 1, keepdim=True)
        #inp = inp + states_X
        #inp = self.inp_ln(inp)
        output = self.prog_pred(inp)
        if inp_state is not None:
            inp_state = self.state_embed_ff(self.state_embed(inp_state))
            inp_state = inp_state + F.normalize(var_idx_embed_orig) + F.normalize(rule_len)
            inp_state = inp_state.unsqueeze(1)
            inp_state = torch.cat((inp_state, inp), 2)
            pred_state = self.state_pred(inp_state)
            return output, torch.sigmoid(pred_state.reshape(-1))
        return output

class TransformerModel(nn.Module):
    def __init__(self, ninp, nhead, nhid, nlayers, dropout=0.0):
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        super(TransformerModel, self).__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.ninp = ninp
        self.decoder = nn.Linear(ninp, ninp)
        self.init_weights()
        self.src_mask = None

    def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self):
        initrange = 0.1
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src):
        if self.src_mask is None or self.src_mask.size(0) != src.size(0):
            device = src.device
            mask = self.generate_square_subsequent_mask(src.size(0)).to(device)
            self.src_mask = mask

        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, self.src_mask)
        output = self.decoder(output)
        return output

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        pe = torch.zeros(5000, d_model)
        position = torch.arange(0, 5000, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class NoamOpt(object):
    def __init__(self, model_size, warmup, optimizer):
        self.optimizer = optimizer
        self._step = 0
        self.warmup = warmup
        self.model_size = model_size
        self._rate = 0

    def state_dict(self):
        return {k: v for k, v in self.__dict__.items() if k != 'optimizer'}

    def load_state_dict(self, state_dict):
        self.__dict__.update(state_dict)

    def step(self):
        self._step += 1
        rate = self.rate()
        for p in self.optimizer.param_groups:
            p['lr'] = rate
        self._rate = rate
        self.optimizer.step()

    def rate(self, step=None):
        if step is None:
            step = self._step
        return (self.model_size ** (-0.5) * min(step ** (-0.5), step * self.warmup ** (-1.5)))


class TransitionDataset(Dataset):
    def __init__(self, expected_size):
        self.conn = sqlite3.connect(dataset_name)
        self.conn.execute('PRAGMA journal_mode=off')
        self.conn.execute('PRAGMA query_only=TRUE')
        #self.conn.execute('PRAGMA CACHE_SIZE = -128000')
        #self.conn.execute('PRAGMA threads = 2')
        #self.conn.execute(f'PRAGMA mmap_size = {2*1024*1024*1024}')
        #self.conn.execute('PRAGMA temp_store = 2')
        #self.conn.execute('PRAGMA page_size = 64000')
        self.expected_size = expected_size

    def __len__(self):
        return self.expected_size
        #self.cur.execute('SELECT COUNT(*) FROM logic_programs')
        #row = self.cur.fetchone()
        #return min(row[0], self.expected_size)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            assert False
            idx = idx.tolist()

        try:
            self.cur = self.conn.cursor()
            self.cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (idx+1,))
            prog = self.cur.fetchone()
            self.cur.execute(f'SELECT state1, state2 FROM transitions WHERE prog_id = ? LIMIT {int(2**num_vars * 0.95)}', (idx+1,))
            data = self.cur.fetchall()
            #np.random.shuffle(data)
            if not prog:
                assert False, f'{idx}: {len(data)}'
            assert len(data) == int(2**num_vars * 0.95)
            return (torch.tensor(data), torch.tensor(pickle.loads(prog[0]), dtype=torch.float))
        except Exception:
            logging.exception('Failed to load')
            self.conn.close()
            time.sleep(10)
            self.conn = sqlite3.connect(dataset_name)
            return self.__getitem__(idx)


def generate_single_datapoint():
    state_transitions = {}
    for i in range(2**(num_vars*delays)):
        state_transitions[i] = np.random.randint(0, 2**num_vars)
    p = [(i, []) for i in range(num_vars)]
    for e_i, e_j in state_transitions.items():
        for a in range(num_vars*delays):
            if e_j & (1 << int(math.floor(a/delays))) == 0:
                rule = (int(math.floor(a/delays)), [
                    (0 if e_i & (1 << b) != 0 else 1,
                     (b, 1))
                    for b in range(num_vars*delays)
                ])
                conflicts = []
                for r in p:
                    if r[0] == rule[0] and subsumes(r[1], rule[1]) and set(r[1]) != set(rule[1]):
                        conflicts.append(r)
                for r in conflicts:
                    p.remove(r)
                for r in conflicts:
                    for l in rule[1]:
                        pos_l = (l[0], (l[1][0], 1))
                        neg_l = (0 if l[0] else 1, (l[1][0], 1))
                        if pos_l not in r[1] and neg_l not in r[1]:
                            r_c = (r[0], [(x[0], (x[1][0], 1)) for x in r[1]])
                            r_c[1].append(neg_l)
                            if not prog_subsumes(p, r_c):
                                p = [
                                    r_p for r_p in p
                                    if (
                                            r_c[0] == r_p[0] and not subsumes(r_c[1], r_p[1])
                                    ) or r_c[0] != r_p[0]
                                ]
                                p.append(r_c)
    new_p = []
    for r in p:
        new_r = []
        for l in r[1]:
            new_r.append((l[0], (l[1][0] // delays, (l[1][0] % delays)+1)))
        new_p.append((r[0], new_r))
    #print(human_program(new_p, '', num_vars*delays, GENERATE_READABLE))
    one_hot_prog = convert_program_to_one_hot(p, num_vars*delays)[:num_vars]
    one_hot_prog = one_hot_prog.reshape(-1)
    prog_hash = hashlib.md5((':'.join([str(r) for r in one_hot_prog])).encode('utf-8')).digest()
    return state_transitions, one_hot_prog, prog_hash

'''
conn = sqlite3.connect(dataset_name)
cur = conn.cursor()
cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (2,))
prog = cur.fetchone()
prog = pickle.loads(prog[0])
prog = [index_to_prog(j, num_vars) for j, r in enumerate(prog) if r > 0]
print(human_program(prog, '', num_vars, GENERATE_READABLE))
import sys
sys.exit(0)
'''

def generate_dataset(seed, can_stop):
    np.random.seed(seed)
    r = redis.Redis(host='localhost', port=6379)
    try:
        while not can_stop.is_set():
            r.rpush('generate-queue', pickle.dumps(generate_single_datapoint()))
    except KeyboardInterrupt:
        return


if generate_only:
    can_stop = Event()
    processes = []
    for j in range(os.cpu_count() - 2):
        seed = int(j + time.time() % 10000)
        p = Process(target=generate_dataset, args=(seed, can_stop))
        p.start()
        processes.append(p)

    try:
        while True:
            time.sleep(30)
    except KeyboardInterrupt:
        can_stop.set()
        for p in processes:
            p.join()
        import sys
        sys.exit(0)


def generate():
    if has_s3:
        import boto3
        s3 = boto3.resource('s3')

    conn = sqlite3.connect(dataset_name)
    #conn.execute('PRAGMA journal_mode = \'WAL\'')
    #conn.execute('PRAGMA temp_store = 2')
    #conn.execute('PRAGMA synchronous = 1')
    #conn.execute('PRAGMA cache_size = -64000')
    if generate_reset:
        conn.execute('DROP TABLE IF EXISTS transitions')
        conn.execute('DROP TABLE IF EXISTS logic_programs')
        conn.execute('CREATE TABLE transitions (id INTEGER PRIMARY KEY, state1 INTEGER, state2 INTEGER, prog_id INTEGER)')
        conn.execute('CREATE TABLE logic_programs (id INTEGER PRIMARY KEY, hash TEXT UNIQUE, one_hot_prog BLOB)')
        conn.execute('CREATE INDEX prog_id_idx ON transitions (prog_id)')
        conn.commit()
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM logic_programs')
    row = cur.fetchone()
    generated = row[0]

    seen_hashes = set()

    with Manager() as manager:
        can_stop = Event()
        processes = []
        for j in range(os.cpu_count() - 2):
            seed = int(j + time.time() % 10000)
            p = Process(target=generate_dataset, args=(seed, can_stop))
            p.start()
            processes.append(p)

        r = redis.Redis(host='localhost', port=6379)

        pbar = tqdm(total=train_size + val_size + test_size, initial=generated)
        errors = 0
        while generated < train_size + val_size + test_size:
            data = r.blpop('generate-queue')
            state_transitions, one_hot_prog, prog_hash = pickle.loads(data[1])
            if prog_hash in seen_hashes:
                continue
            seen_hashes.add(prog_hash)
            pbar.set_postfix(size=r.llen('generate-queue'), errors=errors)
            try:
                cur = conn.cursor()
                cur.execute('INSERT INTO logic_programs (hash, one_hot_prog) VALUES (?, ?)', (prog_hash, pickle.dumps(one_hot_prog)))
                conn.executemany(f'INSERT INTO transitions (state1, state2, prog_id) VALUES (?, ?, {cur.lastrowid})', state_transitions.items())
                generated += 1
                if generated % 50000 == 0:
                    conn.commit()
                pbar.update(1)
                if generated % 5000 == 0 and has_s3:
                    s3.Object('phua-tmp', 'model5_dataset_004.db').upload_file(dataset_name)
            except Exception:
                logging.exception('Exception')
                errors += 1
                pbar.set_postfix(size=r.llen('generate-queue'), errors=errors)
            if generated + r.llen('generate-queue') > (train_size + val_size + test_size) * 1.1:
                can_stop.set()

        can_stop.set()
        for p in processes:
            p.join()

        pbar.close()

    conn.commit()
    conn.close()
    print('Done')


def to_tuple(lst):
    return tuple(to_tuple(i) if isinstance(i, list) else i for i in lst)

'''
len_pos_weights = {}
for i in range(num_vars+1):
    len_pos_weights[i] = torch.zeros(max_rule_len, dtype=torch.float)
    if i > 0:
        curr_len = length(i, num_vars) - length(i-1, num_vars)
    else:
        curr_len = 1
    len_pos_weights[i][:curr_len] = 1.
    len_pos_weights[i] = len_pos_weights[i].to(device)
'''


from data import index_to_prog


if do_params:
    from matplotlib import pyplot as plt
    x = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
    y = [12281612,12294488,12348880,12428896,12804144,13339296,16016960,19764800,39316096,66362944,211390848,410512320,1498340096,2984410688,11210531968,22408794304]
    plt.plot(x, y, label='With Sharing')
    y = [12289307,12323729,12432499,12720793,13638795,16130529,24045923,45920105,115024123,308655409,916599123,2638816569,8014233707]
    plt.plot(x[:-3], y, label='Without Sharing')
    plt.xlabel('Number of Variables')
    plt.ylabel('Parameters')
    plt.legend()
    plt.savefig('params.png')
    import sys
    sys.exit(0)
    model = SetTransformer(tuple_length, 1, max_rule_len, dim_hidden=1024,
                           num_heads=24, ln=True)
    pytorch_total_params = sum(p.numel() for p in model.parameters())
    print(pytorch_total_params)


idx_to_rule_dict = {}
for i in range(length(num_vars*delays, num_vars*delays)):
    rules = []
    b = index_to_rule(i, num_vars*delays)
    for a in b:
        rules.append(to_tuple(a))
    idx_to_rule_dict[i] = rules


idxes = [(0, 1)]
for i in range(1, num_vars*delays+1):
    idxes.append((length(i-1, num_vars*delays), length(i, num_vars*delays)))

rule_lengths = [1]
for i in range(1, num_vars*delays+1):
    rule_lengths.append(length(i, num_vars*delays))

max_tensor = torch.ones(batch_size, max_rule_len, device=device)*1.0


def get_batch(data, rule_len, var_idx, is_evaluate=False):
    batch_size = data[0].shape[0]
    var_idx_in = torch.tensor(var_idx)
    rules_idx = torch.stack([torch.arange(a * (3 ** (num_vars*delays)) + rule_lengths[rule_len-1], a * (3 ** (num_vars*delays)) + rule_lengths[rule_len], device=device) for a in var_idx])
    var_idx = torch.tensor([1 << a for a in var_idx])
    inp_state = data[0][:,-1]
    inp_state[:,1] = (inp_state[:,1] & var_idx) > 0
    var_idx = var_idx.unsqueeze(1).expand(batch_size, data[0].shape[1])
    inp = data[0][:,:,0] + (2 ** (num_vars*delays)) * ((data[0][:,:,1] & var_idx) > 0)
    target = data[1].to(device=device)
    rules_idxes = torch.gather(target, 1, rules_idx)[:,:max_rule_len]
    targets = []
    targets_true = []
    start_idx = idxes[rule_len][0]
    end_idx = idxes[rule_len][1]
    prog = torch.zeros(batch_size, max_rule_len, dtype=torch.float, device=device)
    prog_true = torch.zeros(batch_size, max_rule_len, dtype=torch.float, device=device)
    if rules_idxes.shape[1] < max_rule_len:
        rules_idxes = torch.cat((rules_idxes, torch.zeros(batch_size, max_rule_len - rules_idxes.shape[1], device=device)), 1)
    rule_idxes = rules_idxes.bool()
    prog[rule_idxes] = 1
    prog_true[rule_idxes] = 1
    subs2 = subsumption_mat[:,start_idx:end_idx].clone().to(device)
    subs2 = subs2.unsqueeze(0).repeat(batch_size*2, 3 ** num_vars, end_idx-start_idx)
    subs = torch.zeros(batch_size, max_rule_len, device=device)
    subs3 = torch.zeros(batch_size, max_rule_len, device=device)
    if True:
        for i in range(batch_size):
            rules_t = data[1][i][var_idx_in[i].item() * (3 ** (num_vars*delays)):(var_idx_in[i].item()+1) * (3 ** (num_vars*delays))].to(dtype=torch.bool)
            t = 0
            if not is_evaluate and len(rule_idxes) == 0:
                subs2[i*2,~rules_t,:] = 0
                subs[i,:end_idx-start_idx] = torch.sum(subs2[i*2], dim=0)
                subs2[i*2+1,:,~rules_t] = 0
                subs3[i,:end_idx-start_idx] = torch.sum(subs2[i*2+1], dim=1)
                valid_idx = subs3[i].nonzero().view(-1)
                if len(valid_idx) > 1:
                    choice = torch.multinomial(valid_idx.float(), min((num_vars*delays)*2, len(valid_idx)))
                    subs3[i,~valid_idx[choice]] = 0
                subs[i] += subs3[i]
                subs[i, prog[i].to(dtype=torch.bool)] = 0
        if not is_evaluate:
            subs = torch.min(subs*1.0, max_tensor)
            prog += subs
    targets = prog
    targets_true = prog_true
    #total_zeros += len((torch.nonzero(targets, as_tuple=True)[1] == 0).nonzero(as_tuple=True)[0])
    #print(rule_len, (torch.nonzero(targets, as_tuple=True)[1] == 0).nonzero(as_tuple=True)[0])
    #rule_lens = F.one_hot(torch.tensor(rule_len), num_vars+1)
    #rule_lens = rule_lens.unsqueeze(0).expand(batch_size, num_vars+1)
    rule_lens = torch.tensor(rule_len).unsqueeze(0).expand(batch_size)
    return inp[:,:-1].to(device), targets.to(device), targets_true.to(device), rule_len, rule_lens.to(device), var_idx_in.to(device), inp_state[:,0].to(device), inp_state[:,1].to(device, dtype=torch.float)

if do_generate:
    generate()

if do_stats:
    print('Doing stats...')
    conn = sqlite3.connect(dataset_name)
    conn.execute('PRAGMA CACHE_SIZE = 1000000')
    #conn.execute('PRAGMA threads = 2')
    #conn.execute(f'PRAGMA mmap_size = {2*1024*1024*1024}')
    conn.execute('PRAGMA temp_store = MEMORY')
    cur = conn.cursor()
    #stats = {}
    #count = {}
    pos_stats = torch.zeros(max_rule_len, device=device)
    neg_stats = torch.zeros(max_rule_len, device=device)
    count = 0
    #for rule_len in range(1, 5):
    #    stats[rule_len] = [torch.zeros(max_rule_len) for _ in range(num_vars)]
    #    count[rule_len] = [0 for _ in range(num_vars)]
    pbar = tqdm(total=train_size + val_size + test_size)

    all_rules_idx = [[]] * (num_vars*delays+1)
    for rule_len in range(1, num_vars*delays+1):
        all_rules_idx[rule_len] = torch.stack([torch.arange(rule_lengths[rule_len-1], rule_lengths[rule_len]) for a in range(num_vars)])
    try:
        i = 0
        for prog in cur.execute('SELECT one_hot_prog FROM logic_programs'):
            prog = torch.tensor(pickle.loads(prog[0]), dtype=torch.float)
            #print(prog)
            prog = prog.view(num_vars, length(num_vars*delays, num_vars*delays))
            for rule_len in range(1, num_vars*delays+1):
                rules_idxes = torch.gather(prog, 1, all_rules_idx[rule_len])[:,:max_rule_len]
                for var_idx in range(num_vars):
                    rule_idxes = torch.nonzero(rules_idxes[var_idx], as_tuple=True)[0]
                    #stats[rule_len][var_idx][rule_idxes] += 1
                    #count[rule_len][var_idx] += 1
                    pos_stats[rule_idxes] += 1
                    neg_stats[~rule_idxes] += 1
                    count += 1
            pbar.update(1)
            i += 1
            if i > train_size:
                break
    except KeyboardInterrupt:
        pass
    pbar.close()
    conn.close()

    '''
    for rule_len in range(1, 5):
        for var_idx in range(num_vars):
            count[rule_len][var_idx] = torch.full(stats[rule_len][var_idx].shape, count[rule_len][var_idx], dtype=torch.long)
            neg = count[rule_len][var_idx] - stats[rule_len][var_idx]
            stats[rule_len][var_idx][stats[rule_len][var_idx] == 0] = 1
            pos_weights = neg / stats[rule_len][var_idx]
            print(pos_weights)
            torch.save(pos_weights, f'pos_weights_5_{rule_len}_{var_idx}.pt')
    '''
    #count = torch.full(stats.shape, count, dtype=torch.long, device=device)
    #neg = count - stats
    pos_weights = neg_stats / pos_stats
    print(pos_weights)
    torch.save(pos_weights, 'pos_weights_8.pt')
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(figsize =(10, 7))
    ax.bar(range(max_rule_len), pos_weights.cpu().numpy())
    plt.yscale('log')
    plt.savefig('stats-7.png')

rule_len_masks = {}
for rule_len in range(1, num_vars*delays+1):
    mask = torch.zeros(batch_size, max_rule_len, device=device, dtype=torch.bool)
    mask[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])] = 1
    rule_len_masks[rule_len] = mask

if do_train:
    if os.path.isfile(f'subsumption_mat_{num_vars}_{delays}.pt'):
        subsumption_mat = torch.load(f'subsumption_mat_{num_vars}_{delays}.pt')
    else:
        subsumption_mat = torch.zeros(3 ** (num_vars*delays), 3 ** (num_vars*delays), device=device)
        for i in range(3 ** (num_vars*delays)):
            idx_i = idx_to_rule_dict[i]
            for j in range(3 ** (num_vars*delays)):
                idx_j = idx_to_rule_dict[j]
                subsumption_mat[i][j] = 1 if subsumes(idx_i, idx_j) or subsumes(idx_j, idx_i) else 0
        torch.save(subsumption_mat, f'subsumption_mat_{num_vars}_{delays}.pt')

    print('Initializing data set...')
    dataset = TransitionDataset(train_size + val_size + test_size)

    train_data, val_data, test_data = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
    train_dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=2)
    val_dataloader = DataLoader(val_data, batch_size=eval_batch_size, shuffle=True, num_workers=1)
    test_dataloader = DataLoader(test_data, batch_size=eval_batch_size, num_workers=1)

    print('Initializing model...')
    model = SetTransformer(tuple_length, 1, max_rule_len, dim_hidden=512, num_heads=16, ln=True)
    #model = torch.load('model7')
    #optimizer = torch.optim.Adam(model.parameters(), lr=1e-5, weight_decay=1e-4)
    #optimizer = torch.optim.Adadelta(model.parameters(), lr=1e-5, weight_decay=1e-2)
    optimizer = torch.optim.SGD(model.parameters(), lr=1e-5, weight_decay=1e-4)
    #optimizer = torch.optim.Adadelta(model.parameters(), lr=1e-3, weight_decay=1e-6)
    criterions = {}
    pos_weights = torch.load('pos_weights_8.pt').to(device)
    #from matplotlib import pyplot as plt
    #fig, ax = plt.subplots(figsize =(10, 7))
    #ax.bar(range(100), pos_weights[500:600].cpu().numpy())
    #plt.savefig('stats-7-100.png')
    #fig, ax = plt.subplots(figsize =(10, 7))
    #ax.bar(range(max_rule_len), pos_weights.cpu().numpy())
    #plt.yscale('log')
    #plt.savefig('stats-7.png')
    #import sys
    #sys.exit(0)
    train_criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weights)
    #train_criterion = nn.BCEWithLogitsLoss()
    train_criterion_2 = nn.MSELoss()
    val_criterion = nn.BCEWithLogitsLoss()
    '''
    for rule_len in range(1, 5):
        pos_weights = []
        for i in range(num_vars):
            weights = torch.load(f'pos_weights_5_{rule_len}_{i}.pt')
            pos_weights.append(weights)
        pos_weights = torch.sum(torch.stack(pos_weights), dim=0).to(device)
        criterions[rule_len] = nn.BCEWithLogitsLoss(pos_weight=pos_weights[:280])
    '''
    model = model.to(device)

    try:
        count = 0
        for epoch in range(train_epochs):
            model.train()
            losses = []
            running_loss = 0.0
            loss_a = 0.0
            loss_b = 0.0
            loss_c = 0.0
            loss_d = 0.0
            pbar = tqdm(total=train_size / batch_size)
            for batch, batched_data in enumerate(train_dataloader):
                for rule_len in np.random.permutation(range(1, num_vars*delays+1))[:1]:#np.random.randint(1, 4, size=3):
                    var_idx = np.random.randint(num_vars, size=batch_size)
                    #mask = rule_len_masks[rule_len]
                    data, targets, targets_true, rule_len, rule_lens, var_idx, inp_state, inp_state_target = get_batch(batched_data, rule_len, var_idx)
                    preds, pred_states = model(data, rule_lens, var_idx, inp_state)
                    preds = preds.squeeze()
                    #preds[~mask] = 0
                    #preds = torch.mul(preds, mask)
                    #loss = criterions[rule_len](preds, targets)
                    a = train_criterion(preds, targets)
                    b = train_criterion_2(torch.sigmoid(preds[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])]), targets_true[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])])
                    c = train_criterion_2(pred_states, inp_state_target)
                    d = torch.mean(torch.norm(preds[:,:(rule_lengths[rule_len] - rule_lengths[rule_len-1])], dim=1))
                    #loss = 0.1*a + b + 0.001*c + d
                    loss = a

                    optimizer.zero_grad()
                    loss.backward()
                    #nn.utils.clip_grad_norm_(model.parameters(), max_norm=5.0, norm_type=2)
                    #nn.utils.clip_grad_value_(model.parameters(), clip_value=1.0)
                    optimizer.step()

                    losses.append(loss.item())
                    running_loss += loss.item()
                    loss_a += a.item()
                    loss_b += b.item()
                    loss_c += c.item()
                    loss_d += d.item()
                if batch % 5000 == 4999:
                    total_norm = 0
                    for p in model.parameters():
                        if p.grad is not None:
                            param_norm = p.grad.detach().data.norm(2)
                            total_norm += param_norm.item() ** 2
                    total_norm = total_norm ** 0.5
                    writer.add_scalar('grad norm', total_norm, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('training loss', running_loss / 100, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('loss/a', loss_a / 100, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('loss/b', loss_b / 100, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('loss/c', loss_c / 100, epoch * (train_size / batch_size) + batch)
                    writer.add_scalar('loss/d', loss_d / 100, epoch * (train_size / batch_size) + batch)
                    for i in [0, 2]:
                        writer.add_histogram(f'prog_pred/{i}.bias', model.prog_pred[i].bias, epoch * (train_size / batch_size) + batch)
                        writer.add_histogram(f'prog_pred/{i}.weight', model.prog_pred[i].weight, epoch * (train_size / batch_size) + batch)
                        writer.add_histogram(f'prog_pred/{i}.weight.grad', model.prog_pred[i].weight.grad, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'state_embed_ff/bias', model.state_embed_ff.bias, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'state_embed_ff/weight', model.state_embed_ff.weight, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'state_embed_ff/weight.grad', model.state_embed_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'rule_ff/bias', model.rule_ff.bias, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'rule_ff/weight', model.rule_ff.weight, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'rule_ff/weight.grad', model.rule_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'var_idx_ff/bias', model.var_idx_ff.bias, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'var_idx_ff/weight', model.var_idx_ff.weight, epoch * (train_size / batch_size) + batch)
                    #writer.add_histogram(f'var_idx_ff/weight.grad', model.var_idx_ff.weight.grad, epoch * (train_size / batch_size) + batch)
                    running_loss = 0.0
                    loss_a = 0.0
                    loss_b = 0.0
                    loss_c = 0.0
                    loss_d = 0.0
                pbar.update(1)
                pbar.set_postfix(loss=loss.item())
            pbar.close()

            avg_loss = np.mean(losses)
            print(f'Epoch {epoch}: train loss {avg_loss:.3f}')

            model.eval()
            losses = []
            for batched_data in test_dataloader:
                # rule_len = np.random.randint(num_vars+1)
                rule_len = np.random.randint(num_vars*delays) + 1
                var_idx = np.random.randint(num_vars, size=eval_batch_size)
                data, targets, targets_true, rule_len, rule_lens, var_idx, inp_state, inp_state_target = get_batch(batched_data, rule_len, var_idx, True)
                preds, _ = model(data, rule_lens, var_idx, inp_state)
                preds = preds.reshape(preds.shape[0], -1)
                loss = val_criterion(preds, targets_true)
                losses.append(loss.item())

            avg_loss = np.mean(losses)
            writer.add_scalar('test loss', avg_loss, epoch)
            print(f'Epoch {epoch}: test loss {avg_loss:.3f}')
            #break
    except KeyboardInterrupt:
        pass

    '''
    count = torch.full(stats.shape, count, dtype=torch.long, device=device)
    neg = count - stats
    stats[stats == 0] = 1
    pos_weights = neg / stats
    print(pos_weights)
    #torch.save(pos_weights, 'pos_weights_5.pt')
    from matplotlib import pyplot as plt
    stats = stats.cpu().numpy()
    fig, ax = plt.subplots(figsize =(10, 7))
    ax.bar(range(max_rule_len), stats)
    plt.savefig('stats.png')
    '''
    torch.save(model, 'model11_3')

if do_test:
    model = torch.load('model10_75_pct_2')
    model.eval()
    n3a = False
    n3b = False
    raf = False

    if num_vars == 2:
        prog = [
            (0, [(0, (1, 1)), (0, (3, 1))]),
            (1, [(0, (2, 1)), (1, (3, 1))]),
        ]

    if num_vars == 3:
        if n3a:
            # 3-node-a
            prog = [
                (0, [(1, (0, 1)), (1, (1, 1))]),
                (0, [(1, (0, 1)), (1, (2, 1))]),
                (1, [(1, (0, 1)), (0, (1, 1))]),
                (2, [(1, (0, 1))]),
                (2, [(0, (1, 1))]),
            ]
        if n3b:
            # 3-node-b
            prog = [
                (0, [(1, (0, 1))]),
                (0, [(0, (1, 1)), (0, (2, 1))]),
                (1, [(0, (2, 1))]),
                (2, [(0, (1, 1))]),
                (2, [(0, (2, 1))]),
            ]
        if raf:
            # Raf
            prog = [
                (0, [(0, (0, 1)), (0, (1, 1))]),
                (0, [(0, (1, 1)), (0, (2, 1))]),
                (1, [(0, (0, 1))]),
                (1, [(0, (1, 1)), (0, (2, 1))]),
                (2, [(1, (0, 1))]),
                (2, [(1, (2, 1))]),
            ]

    if num_vars == 5:
        prog = [
            (0, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
            (0, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (1, [(0, (2, 1)), (1, (2, 1)), (1, (3, 1))]),
            (1, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (2, [(0, (0, 1)), (0, (1, 1)), (0, (3, 1))]),
            (2, [(0, (0, 1)), (1, (1, 1)), (1, (3, 1))]),
            (3, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
            (3, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
            (3, [(1, (1, 1)), (0, (3, 1))]),
            (4, [(1, (0, 1)), (0, (1, 1)), (0, (2, 1))]),
        ]
        '''
        conn = sqlite3.connect(dataset_name)
        cur = conn.cursor()
        cur.execute('SELECT one_hot_prog FROM logic_programs WHERE id = ?', (7,))
        prog = cur.fetchone()
        prog = pickle.loads(prog[0])
        prog = [index_to_prog(j, num_vars) for j, r in enumerate(prog) if r > 0]
        '''

    if num_vars == 7:
        prog = [
            (0, [(1, (3, 1)), (1, (2, 1))]),
            (0, [(1, (3, 1)), (0, (4, 1))]),
            (0, [(0, (3, 1)), (0, (2, 1)), (1, (4, 1))]),
            (1, [(0, (0, 1)), (1, (2, 1))]),
            (1, [(0, (1, 1)), (1, (2, 1))]),
            (1, [(0, (1, 1)), (1, (0, 1))]),
            (2, [(1, (4, 1)), (1, (0, 1))]),
            (2, [(0, (5, 1)), (0, (0, 1))]),
            (2, [(0, (4, 1)), (0, (0, 1))]),
            (3, [(1, (2, 1)), (0, (1, 1))]),
            (3, [(0, (4, 1)), (1, (1, 1))]),
            (3, [(0, (2, 1)), (1, (1, 1))]),
            (4, [(0, (3, 1)), (1, (2, 1))]),
            (4, [(1, (0, 1)), (0, (3, 1))]),
            (4, [(0, (0, 1)), (1, (2, 1))]),
            (5, [(1, (5, 1)), (0, (0, 1))]),
            (5, [(1, (6, 1)), (0, (5, 1)), (1, (0, 1))]),
            (5, [(0, (6, 1)), (0, (0, 1))]),
            (6, [(1, (4, 1)), (0, (2, 1)), (0, (6, 1))]),
            (6, [(0, (4, 1)), (1, (2, 1)), (0, (6, 1))]),
        ]
        prog = [
            (3, [(0, (3, 1))]),
            (1, [(0, (5, 1)), (0, (3, 1))]),
            (1, [(0, (5, 1)), (0, (1, 1))]),
            (1, [(0, (3, 1)), (0, (2, 1))]),
            (2, [(1, (6, 1))]),
            (5, [(0, (3, 1))]),
            (5, [(0, (2, 1))]),
            (6, [(0, (6, 1))]),
            (6, [(1, (1, 1))]),
            (0, [(1, (5, 1))]),
            (4, [(1, (6, 1))]),
            (4, [(0, (1, 1))]),
        ]

    data = []
    for starting_state in np.random.permutation(2 ** (num_vars*delays)):
        chance = np.random.binomial(1, 0.875, len(prog))
        test_prog = [r for i, r in enumerate(prog) if chance[i] == 1]
        func_str = human_program(test_prog, '', num_vars*delays, GENERATE_PYTHON)
        globals_scope = {
            'generate_transition_steps': generate_transition_steps,
            'num_vars': num_vars*delays,
            'seq_len': 2,
        }
        local = {}
        exec(func_str, globals_scope, local)
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([convert_state_to_numbers([1 if x else 0 for x in transitions[0]]), convert_state_to_numbers([1 if x else 0 for x in transitions[1][:num_vars]])])
    pct = int((2 ** (num_vars*delays))*0.875)
    print('pct', pct)
    data = data[:pct]
    data = [data]

    #print(3 ** num_vars)
    data = torch.tensor(data)
    total_pred = []
    for i in range(num_vars):
        print(f'====================\nVar {i}\n===========================')
        var_idx = torch.full((data.shape[0], data.shape[1]), 1 << i, dtype=torch.long)
        var_idx_in = torch.full((num_vars*delays+1,), i, dtype=torch.long, device=device)
        in_data = torch.clone(data)
        in_data[:,:,1] = (in_data[:,:,1] & var_idx) > 0
        inp = in_data[:,:,0] + (2 ** (num_vars*delays)) * in_data[:,:,1]
        in_data = inp.to(device)
        #in_data = in_data.to(device)
        rule_len = torch.arange(num_vars*delays+1).to(device, dtype=torch.long)
        in_data = in_data.expand(num_vars*delays+1, data.shape[1])
        preds_out = model(in_data, rule_len, var_idx_in)
        for rule_len in range(1, num_vars*delays+1):
            preds_out[rule_len][~rule_len_masks[rule_len][:1,:]] = 0
        print(torch.topk(torch.sigmoid(preds_out), 5, dim=2).indices)
        preds = [torch.tensor([-999], device=device)]
        #for i in range(0, num_vars+1):
        for i in range(1, num_vars*delays+1):
        #for i in range(1, 4):
            if i > 0:
                t = preds_out[i,0,:length(i, num_vars*delays)-length(i-1, num_vars*delays)]
            else:
                t = preds_out[i,0,:1]
            preds.append(t)
        #preds.append(torch.full((length(num_vars*delays, num_vars*delays) - length(max_rule_len_x, num_vars*delays),), -999, dtype=torch.float).to(device))
        preds = torch.cat(preds)
        print(torch.sigmoid(preds))
        print(torch.topk(preds, 10))
        total_pred.append(preds)
    output = torch.stack(total_pred)
    total_pred = torch.cat(total_pred)
    preds = total_pred.unsqueeze(0)
    #print(torch.sigmoid(preds[0]).tolist()[6*(3**(num_vars*delays)):])
    targets = torch.tensor([convert_program_to_one_hot(prog, num_vars*delays)[:num_vars]], dtype=torch.float).to(device)
    targets = targets.reshape(1, -1)
    criterion = nn.BCEWithLogitsLoss()
    val = criterion(preds, targets)
    print(val.item())
    #print(output_idx)
    print(preds.shape)
    print('Sigmoid stuff:')
    print(torch.topk(torch.sigmoid(output), num_vars, dim=1))
    output_idx = (torch.sigmoid(output) >= 0.9).bool()
    #output[~output_idx] = 0
    #output[output_idx] = 1
    test_out = torch.sigmoid(output)
    #test_out[test_out < 0.5] = 0
    output_2 = torch.topk(test_out, num_vars, dim=1)
    print('Original stuff:')
    print(output_2)
    output_idx = output_2.indices
    print(output_idx)
    output = torch.zeros_like(output, device='cpu')
    for i, idxes in enumerate(output_idx.tolist()):
        output[i][idxes] = 1
    print(output.shape)
    output = output.reshape(-1)
    #pred_prog_rules = [(i, []) for i in range(num_vars)]
    pred_prog_rules = []
    l = list(enumerate(output))
    for i, r in l:
        if r:
            new_r = index_to_prog(i, num_vars*delays)
            new_r[1] = to_tuple(new_r[1])
            conflicts = []
            new_body = set(new_r[1])
            var_in_rules = set([b[1][0] for b in new_r[1]])
            to_add = set([to_tuple(new_r)])
            while len(to_add) > 0:
                new_add = set()
                for new_r in to_add:
                    do_append = True
                    do_add = True
                    post_pred_prog_rules = []
                    for a in pred_prog_rules:
                        if new_r[0] == a[0] and subsumes(a[1], new_r[1]):
                            post_pred_prog_rules.append(a)
                            do_append = False
                            do_add = False
                        elif new_r[0] == a[0] and subsumes(new_r[1], a[1]):
                            do_append = True
                        elif new_r[0] == a[0] and len(new_body.intersection(set(a[1]))) > 0:
                            intersected_vars = set([v[1][0] for v in new_body.intersection(set(a[1]))])
                            a_var_in_rules = set([b[1][0] for b in a[1]])
                            new_left = var_in_rules - intersected_vars
                            a_left = a_var_in_rules - intersected_vars
                            if new_left == a_left:
                                a_rule = (a[0], to_tuple(new_body.intersection(set(a[1]))))
                                new_add.add(a_rule)
                                do_append = False
                                do_add = False
                            else:
                                post_pred_prog_rules.append(a)
                        else:
                            post_pred_prog_rules.append(a)
                    if do_append:
                        post_pred_prog_rules.append(new_r)
                    elif do_add:
                        new_add.add(new_r)
                    pred_prog_rules = post_pred_prog_rules
                to_add = new_add
    print('Predicted:')
    new_p = []
    for r in pred_prog_rules:
        new_r = []
        for l in r[1]:
            new_r.append((l[0], (l[1][0] % num_vars, (l[1][0] // num_vars)+1)))
        new_p.append((r[0], new_r))
    print(human_program(new_p, '', num_vars, GENERATE_READABLE))
    print('Truth:')
    new_p = []
    for r in prog:
        new_r = []
        for l in r[1]:
            new_r.append((l[0], (l[1][0] % num_vars, (l[1][0] // num_vars)+1)))
        new_p.append((r[0], new_r))
    print(human_program(new_p, '', num_vars, GENERATE_READABLE))

    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 2,
    }
    local = {}
    exec(func_str, globals_scope, local)
    data = []
    for starting_state in range(2 ** (num_vars*delays)):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
    truth_data = np.array(data)

    func_str = human_program(pred_prog_rules, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 2,
    }
    local = {}
    exec(func_str, globals_scope, local)
    data = []
    for starting_state in range(2 ** (num_vars*delays)):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
    pred_data = np.array(data)
    #print(truth_data.tolist())
    #print(pred_data.tolist())
    for i, s in enumerate(truth_data):
        print(s[0], s[1], pred_data[i][1])
    print((np.square(pred_data[:,1,:num_vars] - truth_data[:,1,:num_vars]).mean()))
    #print((np.square(pred_data[:,1] - truth_data[:,1]).mean()))
