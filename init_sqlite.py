import sqlite3
import os
import pickle
import sklearn
import traceback
from concurrent.futures import ProcessPoolExecutor, wait
from multiprocessing import Manager


conn = sqlite3.connect('dataset.db')
#for row in conn.execute('SELECT rowid, id FROM training_data ORDER BY rowid ASC'):
#    print(row[0], row[1])
#import sys
#conn.execute('UPDATE training_data SET id = NULL WHERE id IS NOT NULL')
#conn.commit()
#sys.exit(0)
#conn.execute('ALTER TABLE training_data ADD COLUMN id INTEGER')
#conn.execute('CREATE UNIQUE INDEX idx_training_data_id ON training_data (id)')
#conn.commit()
cur = conn.cursor()
cur.execute('SELECT COUNT(*) FROM training_data')
count = cur.fetchone()[0]
conn.close()
def fill(i, l):
    print(i)
    conn = sqlite3.connect('dataset.db')
    c = conn.cursor()
    cu = 0
    params = []
    try:
        for row in c.execute(f'SELECT rowid FROM training_data ORDER BY rowid LIMIT 5000 OFFSET {i*5000}'):
            params.append((i*5000 + cu, row[0]))
            cu += 1
        with l:
            conn.executemany('UPDATE training_data SET id = ? WHERE rowid = ?', params)
            conn.commit()
        conn.close()
    except Exception:
        traceback.print_exc()
with Manager() as manager:
    l = manager.Lock()
    with ProcessPoolExecutor() as executor:
        futures = []
        steps = count // 5000 + 1
        for i in range(steps):
            futures.append(executor.submit(fill, i, l))
import sys
sys.exit(0)
conn.execute('UPDATE training_data SET id = (SELECT COUNT(*) FROM training_data t2 WHERE t2.rowid <= training_data.rowid)')
conn.commit()
import sys
sys.exit(0)
conn.execute('''
CREATE TABLE training_data (data BLOB)
''')
conn.close()

def convert_dataset(f, l):
    try:
        print(f)
        conn = sqlite3.connect('dataset.db')
        with open(os.path.join('dataset', f), 'rb') as n:
            dataset, _ = pickle.load(n)
        inserts = []
        for j, d in enumerate(dataset):
            inserts.append((pickle.dumps(d),))
            if len(inserts) >= 5000:
                with l:
                    conn.executemany('INSERT INTO training_data (data) VALUES (?)',
                                     inserts)
                    conn.commit()
                    inserts = []

        if len(inserts) > 0:
            with l:
                conn.executemany('INSERT INTO training_data (data) VALUES (?)',
                                 inserts)
                conn.commit()
        conn.close()
    except Exception as e:
        traceback.print_exc()

with Manager() as manager:
    l = manager.Lock()
    with ProcessPoolExecutor() as executor:
        futures = []
        files = os.listdir('dataset/')
        for f in files:
            futures.append(executor.submit(convert_dataset, f, l))

        wait(futures)
