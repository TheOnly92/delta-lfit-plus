import numpy as np

num_vars = 3

def hamming_distance(x, y):
    r = (1 << np.arange(2 ** num_vars))[:,None]
    return np.count_nonzero((np.bitwise_xor(x, y) & r) != 0)

print(hamming_distance(28, 31))
