import math
import torch
import torch.nn as nn
import torch.nn.functional as F


class TransformerModel(nn.Module):
    def __init__(self, ntoken, ninp, nhead, nhid, nlayers, dropout=0.5):
        super(TransformerModel, self).__init__()
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        self.model_type = 'Transformer'
        self.src_mask = None
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, ninp)
        self.ninp = ninp
        self.ln_1 = nn.LayerNorm(nhid)
        self.ln_2 = nn.LayerNorm(nhid)
        self.decoder = nn.Linear(nhid, ntoken, bias=False)
        self.dropout = nn.Dropout(p=dropout)

        #self.init_weights()
        self.apply(self.init_weights)

    def _generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self, module):
        if isinstance(module, (nn.Linear, nn.Embedding)):
            module.weight.data.normal_(mean=0.0, std=0.02)
            if isinstance(module, nn.Linear) and module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.LayerNorm):
            module.bias.data.zero_()
            module.weight.data.fill_(1.0)

    '''
    def init_weights(self):
        initrange = 0.02
        self.encoder.weight.data.uniform_(-initrange, initrange)
        #self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)
    '''

    def forward(self, src):
        if self.src_mask is None or self.src_mask.size(0) != len(src):
            device = src.device
            mask = self._generate_square_subsequent_mask(len(src)).to(device)
            self.src_mask = mask

        src = self.encoder(src) * math.sqrt(self.ninp)
        src = self.pos_encoder(src)
        src = self.ln_1(src)
        output = self.transformer_encoder(src, self.src_mask)
        output = self.ln_2(output)
        output = self.decoder(output)
        return output


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
'''
import torchtext
from torchtext.data.utils import get_tokenizer
TEXT = torchtext.data.Field(
    tokenize=get_tokenizer("basic_english"),
    init_token='<sos>',
    eos_token='<eos>',
    lower=True,
)
train_txt, val_txt, test_txt = torchtext.datasets.WikiText2.splits(TEXT)
TEXT.build_vocab(train_txt)

def batchify(data, bsz):
    data = TEXT.numericalize([data.examples[0].text])
    nbatch = data.size(0) // bsz
    data = data.narrow(0, 0, nbatch * bsz)
    data = data.view(bsz, -1).t().contiguous()
    return data.to(device)

batch_size = 20
eval_batch_size = 10
train_data = batchify(train_txt, batch_size)
val_data = batchify(val_txt, eval_batch_size)
test_data = batchify(test_txt, eval_batch_size)

bptt = 20
def get_batch(source, i):
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].view(-1)
    return data, target

print(get_batch(val_data, 0)[0].shape)
print(get_batch(val_data, 0)[1].shape)
import sys
sys.exit(0)
'''

import numpy as np
from data import (
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
)
num_vars = 15
train_size = int(2e6)
val_size = 1000
test_size = 1000
seq_len = 15
generate = False

def convert_state_to_numbers(state):
    return sum([s * (2**i) for i, s in enumerate(state)])

from tqdm import tqdm
from multiprocessing import Event, Process, Manager
from queue import Queue
import os


def generate_data(seed, can_stop, queue, num_vars, seq_len):
    np.random.seed(seed)
    i = 0
    all_zeros = 0
    while not can_stop.is_set():
        prog, m = generate_program(num_vars)
        i += 1
        func_str = human_program(prog, '_min{0}'.format(i), num_vars, GENERATE_PYTHON)
        globals_scope = {
            'generate_transition_steps': generate_transition_steps,
            'num_vars': num_vars,
            'seq_len': seq_len,
        }
        dataset_candidates = []
        for starting_state in np.random.permutation(2 ** num_vars):
            local = {
                'starting_state': starting_state,
            }
            exec(func_str, globals_scope, local)
            exec(
                'transitions = generate_transition_steps(generate_transition_min{0}, starting_state)'
                .format(i),
                globals_scope,
                local,
            )
            transitions = local['transitions']
            p = []
            for v in range(num_vars):
                p.append((v, []))
            transitions = [convert_state_to_numbers([1 if x else 0 for x in t]) for t in transitions]
            if transitions.count(0) >= len(transitions) * 0.8:
                if all_zeros > 3:
                    continue
                all_zeros += 1
            dataset_candidates.append(transitions)
            if len(dataset_candidates) > 3:
                break

        if len(dataset_candidates) >= 3:
            queue.put_nowait((dataset_candidates, hash(func_str)))

import time
import pickle

if generate:
    reuse = True
    dataset = []
    generated_progs = set()
    generated = 0
    if reuse:
        with open('training_data.pickle', 'rb') as f:
            dataset, generated_progs = pickle.load(f)
        generated = len(dataset)
        generated_progs = set(generated_progs)
    with Manager() as manager:
        can_stop = Event()
        queue = manager.Queue()
        processes = []
        for i in range(os.cpu_count()-1):
            p = Process(target=generate_data, args=(int(i + time.time() % 50), can_stop, queue, num_vars, seq_len))
            p.start()
            processes.append(p)

        pbar = tqdm(total=train_size + val_size + test_size)
        pbar.update(generated)
        while generated < train_size + val_size + test_size:
            dataset_candidates, hash_func = queue.get()
            if hash_func in generated_progs:
                continue
            generated_progs.add(hash_func)
            generated += len(dataset_candidates)
            pbar.update(len(dataset_candidates))
            dataset = dataset + dataset_candidates

        can_stop.set()
        for p in processes:
            p.join()

        pbar.close()
    with open('training_data.pickle', 'wb') as f:
        pickle.dump((dataset, list(generated_progs)), f)
    import sys
    sys.exit(0)
else:
    with open('training_data.pickle', 'rb') as f:
        dataset, _ = pickle.load(f)

np.random.shuffle(dataset)
dataset = torch.tensor(dataset)
train_data = dataset[:train_size]
val_data = dataset[train_size:train_size+val_size]
test_data = dataset[train_size+val_size:]

def batchify(data, bsz):
    unique, counts = np.unique(data, return_counts=True)
    unique = sorted(dict(zip(unique, counts)).items(), key=lambda x: x[1], reverse=True)
    print([(a, b / (len(data)*seq_len)) for a, b in unique[:10]])
    nbatch = data.size(0) // bsz
    data = data.narrow(0, 0, nbatch * bsz)
    data = data.view(bsz, -1).t().contiguous()
    return data.to(device)

batch_size = 64
eval_batch_size = 16
train_data = batchify(train_data, batch_size)
val_data = batchify(val_data, eval_batch_size)
test_data = batchify(test_data, eval_batch_size)

bptt = 12
def get_batch(source, i):
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].view(-1)
    return data, target


#ntokens = len(TEXT.vocab.stoi)
ntokens = 2 ** num_vars
emsize = 1280
nhid = 1280
nlayers = 36
nhead = 20
dropout = 0.1
model = TransformerModel(ntokens, emsize, nhead, nhid, nlayers, dropout).to(device)

no_decay = ['bias', 'LayerNorm.weight']
params_decay = [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)]
params_nodecay = [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)]

criterion = nn.CrossEntropyLoss()
lr = 3e-4
#optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=0.1)
optimizer = torch.optim.AdamW([
    {'params': params_decay, 'weight_decay': 1e-6},
    {'params': params_nodecay, 'weight_decay': 0.0},
], lr=lr, betas=(0.9, 0.95))
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 0.1, gamma=0.8)

def train():
    model.train()
    total_loss = 0.
    start_time = time.time()
    #ntokens = len(TEXT.vocab.stoi)
    ntokens = 2 ** num_vars
    log_interval = (len(train_data) // bptt) // 10
    for batch, i in enumerate(range(0, train_data.size(0) - 1, bptt)):
        data, targets = get_batch(train_data, i)
        optimizer.zero_grad()
        output = model(data)
        output_flat = output.view(-1, ntokens)
        #print(output_flat.argmax(1)[:10], targets[:10])
        loss = criterion(output_flat, targets)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        optimizer.step()

        total_loss += loss.item()
        if batch % log_interval == 0 and batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            #print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // bptt, scheduler.get_lr()[0], elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            print('| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // bptt, elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


def evaluate(eval_model, data_source, do_output=False):
    eval_model.eval()
    total_loss = 0.
    #ntokens = len(TEXT.vocab.stoi)
    ntokens = 2 ** num_vars
    correct = 0
    total = 0
    with torch.no_grad():
        for i in range(0, data_source.size(0) - 1, bptt):
            data, targets = get_batch(data_source, i)
            output = eval_model(data)
            output_flat = output.view(-1, ntokens)
            if do_output:
                print(targets[:10])
                print(output_flat.argmax(1)[:10])
                correct += (targets == output_flat.argmax(1)).sum().item()
                total += targets.size(0)
            total_loss += len(data) * criterion(output_flat, targets).item()
    if do_output:
        print(f'Accuracy is {100*correct/total:.2f}%')
    return total_loss / (len(data_source) - 1)


best_val_loss = float("inf")
epochs = 100
best_model = None

test_loss = evaluate(model, test_data)
print('=' * 89)
print('| Pre training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
print('=' * 89)

try:
    for epoch in range(1, epochs + 1):
        epoch_start_time = time.time()
        train()
        val_loss = evaluate(model, val_data)
        print('-' * 89)
        print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time), val_loss, math.exp(val_loss)))
        print('-' * 89)

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_model = model

        #scheduler.step()
except KeyboardInterrupt:
    pass

if best_model:
    test_loss = evaluate(best_model, test_data, True)
    print('=' * 89)
    print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
    print('=' * 89)
    torch.save(best_model, 'best_model')
