import numpy as np
from data import (
    length,
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    GENERATE_READABLE,
    convert_program_to_one_hot,
    subsumes,
    prog_subsumes,
    index_to_prog,
    index_to_rule,
)

num_vars = 5
prog = [
    (0, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
    (0, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
    (1, [(0, (2, 1)), (1, (2, 1)), (1, (3, 1))]),
    (1, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
    (2, [(0, (0, 1)), (0, (1, 1)), (0, (3, 1))]),
    (2, [(0, (0, 1)), (0, (1, 1)), (1, (3, 1))]),
    (3, [(0, (1, 1)), (1, (2, 1)), (1, (3, 1)), (0, (4, 1))]),
    (3, [(0, (1, 1)), (0, (2, 1)), (0, (3, 1)), (1, (4, 1))]),
    (3, [(1, (1, 1)), (0, (3, 1))]),
    (4, [(1, (0, 1)), (0, (1, 1)), (0, (2, 1))]),
]

func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
globals_scope = {
    'generate_transition_steps': generate_transition_steps,
    'num_vars': num_vars,
    'seq_len': 2,
}
local = {}
exec(func_str, globals_scope, local)
data = []
for starting_state in range(2 ** num_vars):
    local['starting_state'] = starting_state
    exec(
        'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
        globals_scope,
        local,
    )
    transitions = local['transitions']
    data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
truth_data = np.array(data)

pred_prog_rules = [
    (0, [(0, (0, 1)), (0, (3, 1))]),
    (0, [(0, (0, 1)), (0, (4, 1))]),
    (1, [(0, (3, 1))]),
    (1, [(0, (2, 1)), (0, (4, 1))]),
    (2, [(0, (4, 1))]),
    (3, [(0, (0, 1))]),
    (3, [(0, (1, 1)), (0, (2, 1))]),
    (3, [(0, (1, 1)), (0, (3, 1))]),
    (4, [(0, (0, 1))]),
    (4, [(0, (1, 1)), (0, (2, 1))]),
]
func_str = human_program(pred_prog_rules, '', num_vars, GENERATE_PYTHON)
globals_scope = {
    'generate_transition_steps': generate_transition_steps,
    'num_vars': num_vars,
    'seq_len': 2,
}
local = {}
exec(func_str, globals_scope, local)
data = []
for starting_state in range(2 ** num_vars):
    local['starting_state'] = starting_state
    exec(
        'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
        globals_scope,
        local,
    )
    transitions = local['transitions']
    data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
pred_data = np.array(data)
print((np.square(pred_data - truth_data).mean()))
