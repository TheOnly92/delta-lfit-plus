from collections import Counter
import time
import traceback
import functools
import math
import hashlib
import sklearn
import sklearn.metrics
import seaborn as sns
import pprint
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import sqlite3


generate = False
reuse = True
do_train = True
do_evaluate = False
do_finetune = False
do_testing = True
best_model_name = 'model_005'


@functools.lru_cache
def load_file(name):
    #print(name)
    with open(name, 'rb') as n:
        dataset, _ = pickle.load(n)
    return dataset


class LFITLayer(nn.Module):
    def __init__(self, num_vars, dropout, ninp):
        super(LFITLayer, self).__init__()
        self.dropout = nn.Dropout(dropout)
        self.mlp = nn.Linear(ninp, num_vars * 3 ** num_vars)
        self.init_weights()

    def init_weights(self):
        initrange = 0.01
        #self.encoder.weight.data.uniform_(-initrange, initrange)
        self.mlp.bias.data.zero_()
        self.mlp.weight.data.uniform_(-initrange, initrange)

    def forward(self, inp):
        output = self.dropout(inp[:,-1,:])
        output = self.mlp(output)
        return output


class MAB(nn.Module):
    def __init__(self, dim_Q, dim_K, dim_V, num_heads, ln=False):
        super(MAB, self).__init__()
        self.dim_V = dim_V
        self.num_heads = num_heads
        self.fc_q = nn.Linear(dim_Q, dim_V)
        self.fc_k = nn.Linear(dim_K, dim_V)
        self.fc_v = nn.Linear(dim_K, dim_V)
        self.ln0 = None
        self.ln1 = None
        if ln:
            self.ln0 = nn.LayerNorm(dim_V)
            self.ln1 = nn.LayerNorm(dim_V)
        self.fc_o = nn.Linear(dim_V, dim_V)

    def forward(self, Q, K):
        Q = self.fc_q(Q)
        K, V = self.fc_k(K), self.fc_v(K)

        dim_split = self.dim_v // self.num_heads
        Q_ = torch.cat(Q.split(dim_split, 2), 0)
        K_ = torch.cat(K.split(dim_split, 2), 0)
        V_ = torch.cat(V.split(dim_split, 2), 0)

        A = torch.softmax(Q_.bmm(K_.transpose(1, 2)) / math.sqrt(self.dim_V), 2)
        O = torch.cat((Q_ + A.bmm(V_)).split(Q.size(0), 0), 2)
        O = O if self.ln0 is None else self.ln0(O)
        O = O + F.relu(self.fc_o(O))
        O = O if self.ln1 is None else self.ln1(O)
        return O


class SAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, ln=False):
        super(SAB, self).__init__()
        self.mab = MAB(dim_in, dim_out, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(X, X)


class ISAB(nn.Module):
    def __init__(self, dim_in, dim_out, num_heads, num_inds, ln=False):
        super(ISAB, self).__init__()
        self.I = nn.Parameter(torch.Tensor(1, num_inds, dim_out))
        nn.init.xavier_uniform_(self.I)
        self.mab0 = MAB(dim_out, dim_in, dim_out, num_heads, ln=ln)
        self.mab1 = MAB(dim_in, dim_out, dim_out, num_heads, ln=ln)

    def forward(self, X):
        H = self.mab0(self.I.repeat(X.size(0), 1, 1), X)
        return self.mab1(X, H)


class PMA(nn.Module):
    def __init__(self, dim, num_heads, num_seeds, ln=False):
        super(PMA, self).__init__()
        self.S = nn.Parameter(torch.Tensor(1, num_seeds, dim))
        nn.init.xavier_uniform_(self.S)
        self.mab = MAB(dim, dim, dim, num_heads, ln=ln)

    def forward(self, X):
        return self.mab(self.S.repeat(X.size(0), 1, 1), X)


class SetTransformer(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output, num_inds=32, dim_hidden=128, num_heads=4, ln=False):
        super(SetTransformer, self).__init__()
        self.enc = nn.Sequential(
            ISAB(dim_input, dim_hidden, num_heads, num_inds, ln=ln),
            ISAB(dim_hidden, dim_hidden, num_heads, num_inds, ln=ln)
        )
        self.dec = nn.Sequential(
            nn.Dropout(),
            PMA(dim_hidden, num_heads, num_outputs, ln=ln),
            nn.Dropout(),
            SAB(dim_hidden, dim_hidden, num_heads, ln=ln),
            nn.Dropout(),
            SAB(dim_hidden, dim_hidden, num_heads, ln=ln),
            nn.Dropout(),
            nn.Linear(dim_hidden, dim_output)
        )

    def forward(self, X):
        return self.dec(self.enc(X))


class TransformerModel(nn.Module):
    def __init__(self, ntoken, ninp, nhead, nhid, nlayers, dropout=0.5):
        super(TransformerModel, self).__init__()
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        self.model_type = 'Transformer'
        self.src_mask = None
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, ninp, padding_idx=PAD_TOKEN)
        self.ninp = ninp
        self.ln_1 = nn.LayerNorm(nhid)
        self.ln_2 = nn.LayerNorm(ninp)
        self.decoder = nn.Linear(ninp, ntoken)#, bias=False)
        self.dropout = nn.Dropout(p=dropout)

        #self.init_weights()
        self.apply(self.init_weights)

    def _generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self, module):
        if isinstance(module, (nn.Linear, nn.Embedding)):
            module.weight.data.normal_(mean=0.0, std=0.02)
            if isinstance(module, nn.Linear) and module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.LayerNorm):
            module.bias.data.zero_()
            module.weight.data.fill_(1.0)

    '''
    def init_weights(self):
        initrange = 0.02
        self.encoder.weight.data.uniform_(-initrange, initrange)
        #self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)
    '''

    def forward(self, src):
        if self.src_mask is None or self.src_mask.size(0) != src.size(1):
            device = src.device
            mask = self._generate_square_subsequent_mask(src.size(1)).to(device)
            self.src_mask = mask

        src = src.transpose(0, 1)
        src = self.encoder(src) * math.sqrt(self.ninp)
        src = self.pos_encoder(src)
        #print(src[:,0,:-10])
        #src = self.ln_1(src)
        hidden = self.transformer_encoder(src, self.src_mask)
        hidden = self.ln_2(hidden)
        #print(hidden[:,0,:-10])
        hidden.transpose_(0, 1)
        output = self.decoder(self.dropout(hidden))
        #print(output[0,:-10,:])
        return output, hidden


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

import numpy as np
from data import (
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    GENERATE_READABLE,
    convert_program_to_one_hot,
    subsumes,
    prog_subsumes,
    index_to_prog,
)
num_vars = 7
train_size = int(4000*1024)
val_size = 1024
test_size = 1024

def convert_state_to_numbers(state):
    return sum([s * (2**i) for i, s in enumerate(state)])

from tqdm import tqdm
from multiprocessing import Event, Process, Manager
from queue import Queue
import os


CLS_TOKEN = 2 ** num_vars
EOS_TOKEN = 2 ** num_vars + 1  # End of system
MASK_TOKEN = 2 ** num_vars + 2
EOT_TOKEN = 2 ** num_vars + 3  # End of transition
PAD_TOKEN = 2 ** num_vars + 4

max_seq_len = 80
#max_seq_len = 10


def generate_data(seed, can_stop, queue, num_vars):
    np.random.seed(seed)
    seen = set()
    rules_count = {}
    while not can_stop.is_set():
        prog, m = generate_program(num_vars, True)
        func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
        globals_scope = {
            'generate_transition_steps': generate_transition_steps,
            'num_vars': num_vars,
            'seq_len': 5,
        }
        local = {}
        exec(func_str, globals_scope, local)
        all_transitions = []
        state_transitions = {}
        for starting_state in np.random.permutation(2 ** num_vars):
            local['starting_state'] = starting_state
            exec(
                'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
                globals_scope,
                local,
            )
            transitions = local['transitions']
            new_transitions = []
            for t in transitions:
                state = convert_state_to_numbers([1 if x else 0 for x in t])
                new_transitions.append(state)
            for i in range(len(new_transitions)-1):
                if new_transitions[i] not in state_transitions:
                    state_transitions[new_transitions[i]] = new_transitions[i+1]
                else:
                    assert state_transitions[new_transitions[i]] == new_transitions[i+1]
            if len(set(new_transitions)) < 3:
                new_transitions = new_transitions[:2+np.random.randint(1)]
            else:
                new_transitions = new_transitions[:2+np.random.randint(2)]
            all_transitions.append(new_transitions)
        max_len = max_seq_len - np.random.randint(3)
        next_counts = Counter(state_transitions.values())
        p = [1 - float(next_counts[s])/float(2 ** num_vars) for s in state_transitions.keys()]
        p = [x/sum(p) for x in p]
        transitions_to_use = np.random.choice(list(state_transitions.keys()), max_len, p=p)
        use_transitions = []
        for t in transitions_to_use:
            st = (t, state_transitions[t])
            contains_st = [s for s in all_transitions if st[0] in s and st[1] in s]
            idx = np.random.choice(len(contains_st))
            use_transitions.append(contains_st[idx])
        all_transitions = use_transitions
        p = []
        for v in range(num_vars):
            p.append((v, []))
        seen_transitions = set()
        for transitions in all_transitions:
            for tr_i in range(1, len(transitions)):
                e_i = transitions[tr_i-1]
                e_j = transitions[tr_i]
                if f'{e_i},{e_j}' in seen_transitions:
                    continue
                seen_transitions.add(f'{e_i},{e_j}')
                for a in range(num_vars):
                    if e_j & (1 << a) == 0:
                        rule = (a, [
                            (0 if e_i & (1 << b) != 0 else 1,
                             (b, 1))
                            for b in range(num_vars)
                        ])
                        conflicts = []
                        for r in p:
                            if r[0] == rule[0] and subsumes(r[1], rule[1]):
                                conflicts.append(r)
                        for r in conflicts:
                            p.remove(r)
                        for r in conflicts:
                            for l in rule[1]:
                                pos_l = (l[0], (l[1][0], 1))
                                neg_l = (0 if l[0] else 1, (l[1][0], 1))
                                if pos_l not in r[1] and neg_l not in r[1]:
                                    r_c = (r[0], [(x[0], (x[1][0], 1)) for x in r[1]])
                                    r_c[1].append(neg_l)
                                    if not prog_subsumes(p, r_c):
                                        p = [
                                            r_p for r_p in p
                                            if (
                                                    r_c[0] == r_p[0] and not subsumes(r_c[1], r_p[1])
                                            ) or r_c[0] != r_p[0]
                                        ]
                                        p.append(r_c)
        one_hot_prog = convert_program_to_one_hot(p, num_vars)
        one_hot_prog = one_hot_prog.reshape(-1)
        rule_idx = [i for i, x in enumerate(one_hot_prog) if x == 1]
        max_rules = sorted(rules_count.items(), key=lambda x: x[1], reverse=True)[:5]
        accept = True
        for r in max_rules:
            if r[0] in rule_idx:
                accept = False
                for a in rule_idx:
                    if a not in rules_count:
                        accept = True
                        break
                break
        if not accept:
            continue
        for i in rule_idx:
            rules_count.setdefault(i, 0)
            rules_count[i] += 1
        prog_hash = hashlib.md5((':'.join([str(r) for r in one_hot_prog])).encode('utf-8')).digest()
        if prog_hash in seen:
            continue
        seen.add(prog_hash)
        dataset = [CLS_TOKEN]
        used_transitions = 0
        for t in all_transitions:
            dataset = dataset + t + [EOT_TOKEN]
            used_transitions += 1
            if used_transitions >= max_len:
                break
        if used_transitions >= max_len * 0.5:
            dataset[-1] = EOS_TOKEN
            dataset = dataset + [PAD_TOKEN] * ((max_seq_len * 5 + 1) - len(dataset))

            queue.put_nowait((dataset, prog_hash, one_hot_prog))

import time
import pickle

if generate:
    dataset = []
    prog_labels = []
    generated_progs = set()
    generated = 0
    all_zeros = 0
    '''
    if reuse:
        with open('training_data.pickle', 'rb') as f:
            dataset, generated_progs, prog_labels = pickle.load(f)
        generated = len(dataset)
        generated_progs = set(generated_progs)
    '''
    dataset = []
    conn = sqlite3.connect('dataset_sl80lfit.db')
    conn.execute('PRAGMA journal_mode = \'WAL\'')
    conn.execute('PRAGMA temp_store = 2')
    conn.execute('PRAGMA synchronous = 1')
    conn.execute('PRAGMA cache_size = -64000')
    i = 0
    if not reuse:
        conn.execute('DROP TABLE IF EXISTS training_data')
        conn.execute('DROP TABLE IF EXISTS generated_progs')
        conn.execute('CREATE TABLE training_data (id INTEGER, data BLOB, one_hot_prog BLOB)')
        conn.execute('CREATE UNIQUE INDEX idx_training_data_id ON training_data (id)')
        conn.execute('CREATE TABLE generated_progs (hash TEXT UNIQUE)')
        conn.commit()
    else:
        cur = conn.cursor()
        cur.execute('SELECT MAX(id) FROM training_data')
        row = cur.fetchone()
        if row:
            i = row[0] + 1
            generated = row[0] + 1
    with Manager() as manager:
        can_stop = Event()
        queue = manager.Queue()
        if False:
            # For testing
            generate_data(1, can_stop, queue, num_vars)
            import sys
            sys.exit(0)
        processes = []
        for j in range(os.cpu_count()-4):
            p = Process(target=generate_data, args=(int(j + time.time() % 500), can_stop, queue, num_vars))
            p.start()
            processes.append(p)

        pbar = tqdm(total=train_size + val_size + test_size)
        pbar.update(generated)
        total_zeros = 0
        total_length = 0
        try:
            while generated < train_size + val_size + test_size:
                dataset_candidates, hash_func, label_candidates = queue.get()
                if hash_func in generated_progs:
                    continue
                cur = conn.cursor()
                cur.execute('SELECT COUNT(*) FROM generated_progs WHERE hash = ?', (hash_func,))
                if cur.fetchone()[0] > 0:
                    continue
                generated_progs.add(hash_func)
                #print([i for i, x in enumerate(label_candidates) if x == 1])
                generated += 1
                pbar.update(1)
                #print(f)
                #print(dataset_candidates)
                #print(dataset_candidates)
                dataset.append((i, pickle.dumps(dataset_candidates), pickle.dumps(label_candidates)))
                i += 1
                prog_labels.append(label_candidates)
                total_zeros += dataset_candidates.count(0)
                total_length += len(dataset_candidates)
                pbar.set_postfix(zeros_pct=total_zeros/total_length*100)
                if len(dataset) >= 10000:
                    # Flush to file
                    try:
                        conn.executemany('INSERT INTO training_data (id, data, one_hot_prog) VALUES (?, ?, ?)', dataset)
                        conn.executemany('INSERT INTO generated_progs (hash) VALUES (?)', [(p,) for p in list(generated_progs)])
                        generated_progs = set()
                        dataset = []
                        prog_labels = []
                        conn.commit()
                    except Exception:
                        traceback.print_exc()
        except KeyboardInterrupt:
            pass

        can_stop.set()
        for p in processes:
            p.join()

        pbar.close()
    if len(dataset) > 0:
        conn.executemany('INSERT INTO training_data (id, data, one_hot_prog) VALUES (?, ?, ?)', dataset)
        conn.executemany('INSERT INTO generated_progs (hash) VALUES (?)', [(p,) for p in list(generated_progs)])
        conn.commit()
        '''
        with open(f'dataset/{time.time()}.pickle', 'wb') as f:
            pickle.dump((dataset, prog_labels), f)
        with open('generated_progs.pickle', 'wb') as f:
            pickle.dump(list(generated_progs), f)
        '''

class TransitionDataset(Dataset):
    def __init__(self, expected_size):
        '''
        self.files = os.listdir('dataset/')
        self.file_length = {}
        for f in self.files:
            dataset = load_file(os.path.join('dataset', f))
            self.file_length[os.path.join('dataset', f)] = len(dataset)
        '''
        self.conn = sqlite3.connect('dataset_sl80lfit.db')
        self.conn.execute('PRAGMA CACHE_SIZE = 10000')
        self.expected_size = expected_size

    def __len__(self):
        cur = self.conn.execute('SELECT COUNT(*) FROM training_data')
        row = cur.fetchone()
        return min(row[0], self.expected_size)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            assert False
            idx = idx.tolist()

        cur = self.conn.execute('SELECT data FROM training_data WHERE id = ?', (idx,))
        data = cur.fetchone()[0]
        return torch.tensor(pickle.loads(data))
        count = 0
        dataset = []
        prog_labels = []
        for k, l in self.file_length.items():
            if count <= idx < count + l:
                dataset = load_file(k)
                return torch.tensor(dataset[idx - count])
            count += l

        print(count, idx)
        assert False

class LFITDataset(Dataset):
    def __init__(self, expected_size):
        '''
        self.files = os.listdir('dataset/')
        self.file_length = {}
        for f in self.files:
            dataset = load_file(os.path.join('dataset', f))
            self.file_length[os.path.join('dataset', f)] = len(dataset)
        '''
        self.conn = sqlite3.connect('dataset_sl80lfit.db')
        self.expected_size = expected_size

    def __len__(self):
        cur = self.conn.execute('SELECT COUNT(*) FROM training_data')
        row = cur.fetchone()
        return min(row[0], self.expected_size)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            assert False
            idx = idx.tolist()

        cur = self.conn.execute('SELECT data, one_hot_prog FROM training_data WHERE id = ?', (idx,))
        row = cur.fetchone()
        data = row[0]
        one_hot_prog = row[1]
        return torch.tensor(pickle.loads(data)), torch.tensor(pickle.loads(one_hot_prog))
        count = 0
        dataset = []
        prog_labels = []
        for k, l in self.file_length.items():
            if count <= idx < count + l:
                dataset = load_file(k)
                return torch.tensor(dataset[idx - count])
            count += l

        print(count, idx)
        assert False

#dataset = np.array(dataset[:train_size + val_size + test_size])
'''
rand_indices = torch.randperm(train_size + val_size + test_size).to(dtype=torch.long, device='cpu')

dataset = torch.tensor(dataset[:train_size + val_size + test_size]).to(device)
#prog_labels = torch.tensor(prog_labels[:train_size + val_size + test_size]).to(device)
prog_labels = None

train_data = dataset[rand_indices[:train_size],:]
#train_label = prog_labels[rand_indices[:train_size]]
train_label = None

val_data = dataset[rand_indices[train_size:train_size+val_size]]
#val_label = prog_labels[rand_indices[train_size:train_size+val_size]]
val_label = None

test_data = dataset[rand_indices[train_size+val_size:]]
#test_label = prog_labels[rand_indices[train_size+val_size:]]
test_label = None

print('Shuffling data')
'''
dataset = TransitionDataset(train_size + val_size + test_size)

batch_size = 32
eval_batch_size = 16

train_data, val_data, test_data = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
train_dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=4)
val_dataloader = DataLoader(val_data, batch_size=eval_batch_size, shuffle=True, num_workers=0)
test_dataloader = DataLoader(test_data, batch_size=eval_batch_size, num_workers=0)

replaced_state = {}

def get_batch(data, is_evaluate=False):
    #data = torch.tensor(data).to(device)
    data = data.clone().detach().to(device)
    #data = data[torch.randperm(data.shape[0])]
    target = data.clone().detach()

    probability_matrix = torch.full(data.shape, 0.15).to(device)
    probability_matrix.masked_fill_((data == CLS_TOKEN) | (data == EOS_TOKEN) | (data == EOT_TOKEN) | (data == PAD_TOKEN), value=0.)
    masked_indices = torch.bernoulli(probability_matrix).bool().to(device)
    target[~masked_indices] = -100
    indices_replaced = torch.bernoulli(torch.full(data.shape, 0.8)).bool().to(device) & masked_indices

    if not is_evaluate:
        max_state = sorted(replaced_state.items(), key=lambda x: x[1], reverse=True)[:100]
        for s in max_state:
            indices_replaced = indices_replaced & (target != s[0])

        # Collect statistics
        for s in data[indices_replaced].tolist():
            replaced_state.setdefault(s, 0)
            replaced_state[s] += 1

    data[indices_replaced] = MASK_TOKEN

    if not is_evaluate:
    #if False:
        indices_random = torch.bernoulli(torch.full(data.shape, 0.5)).bool().to(device) & masked_indices & ~indices_replaced
        random_tokens = torch.randint(2 ** num_vars, data.shape, dtype=torch.long).to(device)
        data[indices_random] = random_tokens[indices_random]

    return data.to(device), target.to(device).reshape(-1)


#ntokens = len(TEXT.vocab.stoi)
ntokens = 2 ** num_vars + 5

emsize = 768
nhid = 1024
nlayers = 8
nhead = 8
'''
emsize = 64
nhid = 256
nlayers = 4
nhead = 4


emsize = 128
nhid = 512
nlayers = 8
nhead = 8
'''

dropout = 0.1
model = TransformerModel(ntokens, emsize, nhead, nhid, nlayers, dropout).to(device)
#model = torch.load(best_model_name)

#model = nn.DataParallel(model)

no_decay = ['bias', 'LayerNorm.weight']
params_decay = [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)]
params_nodecay = [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)]

def hamming_distance(x, y):
    r = (1 << np.arange(2 ** num_vars))[:,None]
    return np.count_nonzero((np.bitwise_xor(x, y) & r) != 0)

ce_loss = nn.CrossEntropyLoss(ignore_index=-100)
distances = torch.zeros([2 ** num_vars, 2 ** num_vars], dtype=torch.float32)
for i in range(2 ** num_vars):
    for j in range(2 ** num_vars):
        if i == j:
            continue
        distances[i][j] = hamming_distance(i, j)
#distances = distances.unsqueeze(0)
#distances = distances.repeat(max_seq_len * 5 + 1, 1, 1)
distances = distances.to(device=device)

lr = 2e-5
#optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=0.1)
optimizer = torch.optim.AdamW([
    {'params': params_decay, 'weight_decay': 0.1},
    {'params': params_nodecay, 'weight_decay': 0.0},
], lr=lr)#, betas=(0.9, 0.95))
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 0.1, gamma=0.8)

def train():
    model.train()
    total_loss = 0.
    start_time = time.time()
    #ntokens = len(TEXT.vocab.stoi)
    #ntokens = 2 ** num_vars
    log_interval = max((len(train_data) // batch_size) // 10, 1)
    #log_interval = 10
    for batch, batched_data in enumerate(train_dataloader):
        data, targets = get_batch(batched_data)
        optimizer.zero_grad()
        output, hidden = model(data)
        output_flat = output.view(-1, ntokens)
        #print(output_flat.argmax(1)[:10], targets[:10])
        loss = ce_loss(output_flat, targets)
        d_loss = torch.matmul(F.softmax(output[:,:,:-5], dim=2), distances)
        loss = loss + d_loss.mean()
        #print(targets.tolist()[:batch_size])
        #print(output_flat.argmax(1).tolist()[:batch_size])
        #print(hidden.tolist()[0])
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        optimizer.step()

        total_loss += loss.item()
        batch_end = time.time()
        if batch % log_interval == 0 and batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            #print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, scheduler.get_lr()[0], elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            print('| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            # print(replaced_state.get(0), replaced_state.get(1023), np.mean(list(replaced_state.values())))
            print(targets[targets != -100].tolist()[:batch_size])
            print(output_flat.argmax(1)[targets != -100].tolist()[:batch_size])
            total_loss = 0
            start_time = time.time()
            #print('output', output_flat[:5].argmax())
            #print('target', targets[:5])


def evaluate(eval_model, data_source, do_output=False):
    eval_model.eval()
    total_loss = 0.
    #ntokens = len(TEXT.vocab.stoi)
    #ntokens = 2 ** num_vars
    correct = 0
    total = 0
    confusion_matrix = torch.zeros(2 ** num_vars, 2 ** num_vars)
    all_targets = None
    all_outputs = None
    with torch.no_grad():
        if do_output:
            pbar = tqdm(total=len(data_source))
        for i, batched_data in enumerate(data_source):
            data, targets = get_batch(batched_data, True)
            output_orig, _ = eval_model(data)
            output = output_orig.view(-1, ntokens)
            #output_flat = output.view(-1, ntokens)
            if do_output:
                non_masked_indices = targets != -100
                output_flat = output.argmax(1)
                #print(output[:20,:])
                non_masked_targets = targets[non_masked_indices]
                non_masked_output = output_flat[non_masked_indices]
                if do_output:
                    if all_targets is not None:
                        all_targets = np.concatenate((all_targets, np.array(non_masked_targets.cpu())))
                    else:
                        all_targets = np.array(non_masked_targets.cpu())
                    if all_outputs is not None:
                        all_outputs = np.concatenate((all_outputs, np.array(non_masked_output.cpu())))
                    else:
                        all_outputs = np.array(non_masked_output.cpu())
                #print('input ', data[0,:20])
                #print('target', targets[:20])
                #print('output', output_flat[:20])
                #print('target', non_masked_targets[:20])
                #print('output', non_masked_output[:20])
                correct_matrix = (non_masked_targets == non_masked_output)
                correct += correct_matrix.sum().item()
                total += torch.nonzero(non_masked_indices).size(0)
                for t, p in zip(non_masked_targets, non_masked_output):
                    confusion_matrix[t.long(), p.long()] += 1
                if do_output:
                    pbar.update(1)
            d_loss = torch.matmul(F.softmax(output_orig[:,:,:-5], 2), distances)
            total_loss += len(data) * (ce_loss(output, targets).item() + d_loss.mean().item())
        if do_output:
            pbar.close()
    if do_output:
        print(f'Accuracy is {100*correct/total:.2f}%')
        plt.figure(figsize=(30,30))
        cf_map = sns.heatmap(confusion_matrix, cmap='Blues')
        cf_map.figure.savefig('cf_map.png')
        print(sklearn.metrics.classification_report(all_targets, all_outputs))
    return total_loss / (len(data_source.dataset) - 1)


best_val_loss = float("inf")
epochs = 100
best_model = None


if do_train:
    try:
        stop = False
        for epoch in range(1, epochs + 1):
            epoch_start_time = time.time()
            try:
                train()
            except KeyboardInterrupt:
                stop = True
            val_loss = evaluate(model, val_dataloader)
            print(val_loss)
            print('-' * 89)
            print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time), val_loss, math.exp(val_loss)))
            print('-' * 89)

            if val_loss < best_val_loss:
                best_val_loss = val_loss
                best_model = model
            torch.save(model, f'model_{epoch:03d}')
            if stop:
                break

            #scheduler.step()
    except KeyboardInterrupt:
        pass
    #pprint.pprint(replaced_state)
else:
    best_model = torch.load(best_model_name)

if do_evaluate:
    if best_model:
        if do_train:
            torch.save(best_model, 'best_model')
        test_loss = evaluate(best_model, test_dataloader, True)
        print('=' * 89)
        print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
        print('=' * 89)

        '''
        with torch.no_grad():
            data = [256, 0, 59, 58, 259, 1, 59, 58, 259, 2, 59, 58, 259, 3, 59, 58, 259, 4, 28, 24, 259, 5, 28, 24, 259, 6, 6, 6, 259, 7, 6, 6, 259, 8, 28, 24, 259, 9, 28, 24, 259, 10, 6, 6, 259, 11, 6, 6, 259, 12, 49, 7, 259, 13, 49, 7, 259, 14, 49, 7, 259, 15, 49, 7, 259, 16, 39, 38, 259, 17, 39, 38, 259, 18, 39, 38, 259, 19, 39, 38, 259, 20, 45, 13, 259, 21, 45, 13, 259, 22, 45, 13, 259, 23, 45, 13, 259, 24, 24, 24, 259, 25, 24, 24, 259, 26, 26, 26, 259, 27, 26, 26, 259, 28, 24, 24, 259, 29, 24, 24, 259, 30, 26, 26, 259, 31, 26, 26, 259, 32, 7, 6, 259, 33, 7, 6, 259, 34, 7, 6, 259, 35, 7, 6, 259, 36, 60, 56, 259, 37, 60, 56, 259, 38, 38, 38, 259, 39, 38, 38, 259, 40, 60, 56, 259, 41, 60, 56, 259, 42, 38, 38, 259, 43, 38, 38, 259, 44, 13, 49, 259, 45, 13, 49, 259, 46, 13, 49, 259, 47, 13, 49, 259, 48, 7, 6, 259, 49, 7, 6, 259, 50, 7, 6, 259, 51, 7, 6, 259, 52, 13, 49, 259, 53, 13, 49, 259, 54, 13, 49, 259, 55, 13, 49, 259, 56, 56, 56, 259, 57, 56, 56, 259, 58, 58, 58, 259, 59, 58, 58, 259, 60, 56, 56, 259, 61, 56, 56, 259, 62, 58, 58, 259, 63, 58, 58, 259, 64, 123, 90, 259, 65, 123, 90, 259, 66, 123, 90, 259, 67, 123, 90, 259, 68, 28, 24, 259, 69, 28, 24, 259, 70, 70, 70, 259, 71, 70, 70, 259, 72, 28, 24, 259, 73, 28, 24, 259, 74, 70, 70, 259, 75, 70, 70, 259, 76, 113, 103, 259, 77, 113, 103, 259, 78, 113, 103, 259, 79, 113, 103, 259, 80, 103, 70, 259, 81, 103, 70, 259, 82, 103, 70, 259, 83, 103, 70, 259, 84, 109, 109, 259, 85, 109, 109, 259, 86, 109, 109, 259, 87, 109, 109, 259, 88, 88, 88, 259, 89, 88, 88, 259, 90, 90, 90, 259, 91, 90, 90, 259, 92, 88, 88, 259, 93, 88, 88, 259, 94, 90, 90, 259, 95, 90, 90, 259, 96, 103, 70, 259, 97, 103, 70, 259, 98, 103, 70, 259, 99, 103, 70, 259, 100, 28, 24, 259, 101, 28, 24, 259, 102, 70, 70, 259, 103, 70, 70, 259, 104, 28, 24, 259, 105, 28, 24, 259, 106, 70, 70, 259, 107, 70, 70, 259, 108, 109, 109, 259, 109, 109, 109, 259, 110, 109, 109, 259, 111, 109, 109, 259, 112, 103, 70, 259, 113, 103, 70, 259, 114, 103, 70, 259, 115, 103, 70, 259, 116, 109, 109, 259, 117, 109, 109, 259, 118, 109, 109, 259, 119, 109, 109, 259, 120, 88, 88, 259, 121, 88, 88, 259, 122, 90, 90, 259, 123, 90, 90, 259, 124, 88, 88, 259, 125, 88, 88, 259, 126, 90, 90, 259, 127, 90, 90, 257]
            for idx_to_mask in [2, 6, 19, 50, 101, 150, 162]:
                answer = data[idx_to_mask]
                data[idx_to_mask] = MASK_TOKEN
                target, _ = best_model(torch.tensor([data]).to(device))
                print('answer', answer)
                print('predict', target.view(-1, ntokens).argmax(1)[idx_to_mask])
        '''

if do_finetune:
    print('Fine tuning...')
    batch_size = 64
    eval_batch_size = 8
    best_model = torch.load(best_model_name)
    lfit_model = LFITLayer(num_vars, 0.1, emsize).to(device)

    dataset = LFITDataset(train_size + val_size + test_size)
    train_data, val_data, test_data = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
    train_dataloader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=0)
    val_dataloader = DataLoader(val_data, batch_size=eval_batch_size, shuffle=True, num_workers=0)
    test_dataloader = DataLoader(test_data, batch_size=eval_batch_size, num_workers=0)

    '''
    num_rules = {}
    for label in train_label:
        label = label.tolist()
        for i, x in enumerate(label):
            if x == 1:
                num_rules.setdefault(i, 0)
                num_rules[i] += label.count(i)
    max_ex = max(num_rules.values())
    pos_weights = []
    for i in range(num_vars * 3 ** num_vars):
        pos_weights.append(min(max_ex / float(num_rules[i] if num_rules.get(i, 0) != 0 else 100.), 1.5))
    pos_weights = torch.tensor(pos_weights)
    criterion_finetune = nn.BCEWithLogitsLoss(pos_weight=pos_weights.to(device))
    '''

    criterion_finetune = nn.BCEWithLogitsLoss(pos_weight=torch.ones([num_vars * 3 ** num_vars]).to(device))
    lr = 1e-5
    optimizer = torch.optim.AdamW(list(lfit_model.parameters()), lr=lr, betas=(0.9, 0.95), weight_decay=0.01)

    def get_batch_finetune(data, targets, is_evaluate=False):
        return data.to(device), targets.to(device)

    def train_finetune():
        lfit_model.train()
        total_loss = 0
        start_time = time.time()
        log_interval = max((len(train_data) // batch_size) // 10, 1)
        for batch, batched_data in enumerate(train_dataloader):
            data, targets = get_batch_finetune(batched_data[0], batched_data[1])
            optimizer.zero_grad()
            _, hidden = best_model(data)
            output = lfit_model(hidden)
            #print(output_flat.argmax(1)[:10], targets[:10])
            loss = criterion_finetune(output, targets)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
            optimizer.step()

            total_loss += loss.item()
            if batch % log_interval == 0 and batch > 0:
                cur_loss = total_loss / log_interval
                elapsed = time.time() - start_time
                #print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, scheduler.get_lr()[0], elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
                print('| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
                # print(replaced_state.get(0), replaced_state.get(1023), np.mean(list(replaced_state.values())))
                total_loss = 0
                start_time = time.time()
                #print('output', output_flat[:5].argmax())
                #print('target', targets[:5])

    def evaluate_finetune(eval_model, data_source, do_output=False):
        eval_model.eval()
        total_loss = 0.
        #ntokens = len(TEXT.vocab.stoi)
        #ntokens = 2 ** num_vars
        correct = 0
        total = 0
        confusion_matrix = torch.zeros(2 ** num_vars, 2 ** num_vars)
        all_targets = None
        all_outputs = None
        has_output = False
        with torch.no_grad():
            if do_output:
                pbar = tqdm(total=len(data_source))
            for i, batched_data in enumerate(data_source):
                data, targets = get_batch_finetune(batched_data[0], batched_data[1], True)
                _, hidden = best_model(data)
                output = eval_model(hidden)
                #output_flat = output.view(-1, ntokens)
                if do_output:
                    if do_output:
                        pbar.update(1)
                val = criterion_finetune(output, targets)
                if not has_output:
                    '''
                    print([i for i, x in enumerate(targets[0]) if x == 1])
                    print(output[0])
                    print(len([i for i, x in enumerate(torch.sigmoid(output[0])) if x >= 0.5]))
                    '''
                    has_output = True
                total_loss += len(data) * val.item()
            if do_output:
                pbar.close()
        if do_output:
            '''
            print(f'Accuracy is {100*correct/total:.2f}%')
            plt.figure(figsize=(30,30))
            cf_map = sns.heatmap(confusion_matrix, cmap='Blues')
            cf_map.figure.savefig('cf_map.png')
            print(sklearn.metrics.classification_report(all_targets, all_outputs))
            '''
        return total_loss / (len(data_source) - 1)

    try:
        stop = False
        val_loss = evaluate_finetune(lfit_model, val_dataloader)
        print('-' * 89)
        print('| valid loss {:5.2f} | valid ppl {:8.2f}'.format(val_loss, math.exp(val_loss)))
        print('-' * 89)
        epochs = 3
        for epoch in range(1, epochs + 1):
            epoch_start_time = time.time()
            try:
                train_finetune()
            except KeyboardInterrupt:
                stop = True
            val_loss = evaluate_finetune(lfit_model, val_dataloader)
            print('-' * 89)
            print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time), val_loss, math.exp(val_loss)))
            print('-' * 89)

            if val_loss < best_val_loss:
                best_val_loss = val_loss
                best_lfit_model = lfit_model
            torch.save(lfit_model, f'lfit_model_{epoch:03d}')
            torch.save(best_model, f'lfit_best_model_{epoch:03d}')
            if stop:
                break

            #scheduler.step()
    except KeyboardInterrupt:
        pass

    test_loss = evaluate_finetune(best_lfit_model, test_dataloader, True)
    print('=' * 89)
    print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
    print('=' * 89)

if do_testing:
    #best_model = torch.load(best_model_name)#torch.load('lfit_best_model_001')
    best_model = torch.load('lfit_best_model_003')
    best_lfit_model = torch.load('lfit_model_003')
    '''
    prog = [
        (4, [(0,(4,1))]),
        (1, [(0,(5,1)), (0,(3,1))]),
        (1, [(0,(5,1)), (0,(1,1))]),
        (1, [(0,(3,1)), (0,(1,1))]),
        (2, [(1,(6,1))]),
        (5, [(0,(3,1))]),
        (5, [(0,(2,1))]),
        (6, [(0,(6,1))]),
        (6, [(0,(1,1))]),
        (0, [(1,(5,1))]),
        (4, [(1,(6,1))]),
        (4, [(0,(1,1))]),
    ]
    '''
    prog = [
        (0, [(1, (3, 1)), (1, (2, 1))]),
        (0, [(1, (3, 1)), (0, (4, 1))]),
        (0, [(0, (3, 1)), (0, (2, 1)), (1, (4, 1))]),
        (1, [(0, (0, 1)), (1, (2, 1))]),
        (1, [(0, (1, 1)), (1, (2, 1))]),
        (1, [(0, (1, 1)), (1, (0, 1))]),
        (2, [(1, (4, 1)), (1, (0, 1))]),
        (2, [(0, (5, 1)), (0, (0, 1))]),
        (2, [(0, (4, 1)), (0, (0, 1))]),
        (3, [(1, (2, 1)), (0, (1, 1))]),
        (3, [(0, (4, 1)), (1, (1, 1))]),
        (3, [(0, (2, 1)), (1, (1, 1))]),
        (4, [(0, (3, 1)), (1, (2, 1))]),
        (4, [(1, (0, 1)), (0, (3, 1))]),
        (4, [(0, (0, 1)), (1, (2, 1))]),
        (5, [(1, (5, 1)), (0, (0, 1))]),
        (5, [(1, (6, 1)), (0, (5, 1)), (1, (0, 1))]),
        (5, [(0, (6, 1)), (0, (0, 1))]),
        (6, [(1, (4, 1)), (0, (2, 1)), (0, (6, 1))]),
        (6, [(0, (4, 1)), (1, (2, 1)), (0, (6, 1))]),
    ]
    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 3,
    }
    local = {}
    exec(func_str, globals_scope, local)
    all_transitions = []
    for starting_state in np.random.permutation(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        new_transitions = []
        for t in transitions:
            state = convert_state_to_numbers([1 if x else 0 for x in t])
            new_transitions.append(state)
        all_transitions.append(new_transitions)
    data = [CLS_TOKEN]
    max_len = 140
    used_transitions = 0
    for t in all_transitions:
        data = data + t + [EOT_TOKEN]
        used_transitions += 1
        if used_transitions >= max_len:
            break
    data[-1] = EOS_TOKEN
    #print(data)
    data = [data]
    data = torch.tensor(data).to(device)
    _, hidden = best_model(data)
    print(hidden.shape)
    output = best_lfit_model(hidden)
    criterion_finetune = nn.BCEWithLogitsLoss(pos_weight=torch.ones([num_vars * 3 ** num_vars]).to(device))
    targets = torch.tensor([convert_program_to_one_hot(prog, num_vars)]).to(device)
    targets = targets.reshape(1, -1)
    val = criterion_finetune(output, targets)
    print('Error', val.item())
    print(max(torch.sigmoid(output).tolist()[0]))
    output = (torch.sigmoid(output) >= 0.75).bool()
    pred_prog_rules = []
    for i, r in enumerate(output[0]):
        if r:
            pred_prog_rules.append(index_to_prog(i, num_vars))
    print(output)
    print(human_program(pred_prog_rules, '', num_vars, GENERATE_READABLE))
    '''
    prog = [
        (0, [(1,(3,1)), (0,(1,1))]),
        (0, [(1,(3,1)), (0,(0,1))]),
        (0, [(0,(1,1)), (0,(0,1))]),
        (1, [(1,(7,1)), (1,(6,1)), (1,(4,1))]),
        (2, [(1,(8,1)), (1,(7,1)), (1,(6,1)), (1,(4,1)), (0,(0,1))]),
        (3, [(0,(6,1))]),
        (4, [(1,(5,1)), (0,(4,1)), (0,(3,1)), (1,(2,1))]),
        (4, [(1,(5,1)), (0,(4,1)), (0,(3,1)), (1,(1,1))]),
        (4, [(1,(5,1)), (0,(4,1)), (1,(2,1)), (1,(1,1))]),
        (4, [(1,(5,1)), (0,(3,1)), (1,(2,1)), (1,(1,1))]),
        (4, [(0,(4,1)), (0,(3,1)), (1,(2,1)), (1,(1,1))]),
        (5, [(0,(9,1))]),
        (6, [(0,(2,1))]),
        (7, [(0,(7,1)), (1,(5,1)), (0,(3,1)), (1,(2,1))]),
        (7, [(0,(7,1)), (1,(5,1)), (0,(3,1)), (1,(1,1))]),
        (7, [(0,(7,1)), (1,(5,1)), (1,(2,1)), (1,(1,1))]),
        (7, [(0,(7,1)), (0,(3,1)), (1,(2,1)), (1,(1,1))]),
        (7, [(1,(5,1)), (0,(3,1)), (1,(2,1)), (1,(1,1))]),
        (8, [(0,(8,1)), (0,(3,1))]),
        (8, [(0,(8,1)), (1,(1,1))]),
        (8, [(0,(3,1)), (1,(1,1))]),
        (9, []),
    ]
    func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
    globals_scope = {
        'generate_transition_steps': generate_transition_steps,
        'num_vars': num_vars,
        'seq_len': 3,
    }
    local = {}
    exec(func_str, globals_scope, local)
    all_transitions = []
    for starting_state in np.random.permutation(2 ** num_vars):
        local['starting_state'] = starting_state
        exec(
            'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
            globals_scope,
            local,
        )
        transitions = local['transitions']
        new_transitions = []
        for t in transitions:
            state = convert_state_to_numbers([1 if x else 0 for x in t])
            new_transitions.append(state)
        all_transitions.append(new_transitions)
    data = [CLS_TOKEN]
    max_len = 50
    used_transitions = 0
    for t in all_transitions:
        data = data + t + [EOT_TOKEN]
        used_transitions += 1
        if used_transitions >= max_len:
            break
    data[-1] = EOS_TOKEN
    data = [data]
    data = torch.tensor(data).to(device)
    _, hidden = best_model(data)
    output = best_lfit_model(hidden)
    output = (torch.sigmoid(output) >= 0.5).bool()
    pred_prog_rules = []
    for i, r in enumerate(output[0]):
        if r:
            pred_prog_rules.append(index_to_prog(i, num_vars))
    print(output)
    print(human_program(pred_prog_rules, '', num_vars, GENERATE_READABLE))
    '''
