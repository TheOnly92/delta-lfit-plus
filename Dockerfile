FROM pytorch/pytorch:2.1.2-cuda12.1-cudnn8-devel

ADD requirements.txt /workspace
RUN pip install -r requirements.txt
