import numpy as np
from data import (
    length,
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    GENERATE_READABLE,
    convert_program_to_one_hot,
    subsumes,
    prog_subsumes,
    index_to_prog,
    index_to_rule,
)


num_vars = 3
delays = 1

prog = [
    (0, [(0, (0, 1)), (0, (1, 1))]),
    (0, [(0, (1, 1)), (0, (2, 1))]),
    (1, [(0, (0, 1))]),
    (1, [(0, (1, 1)), (0, (2, 1))]),
    (2, [(1, (0, 1))]),
    (2, [(1, (2, 1))]),
]

def convert_state_to_numbers(state):
    return sum([s * (2**i) for i, s in enumerate(state)])

func_str = human_program(prog, '', num_vars, GENERATE_PYTHON)
globals_scope = {
    'generate_transition_steps': generate_transition_steps,
    'num_vars': num_vars,
    'seq_len': 2,
}
local = {}
exec(func_str, globals_scope, local)
data = []
for starting_state in range(2 ** (num_vars*delays)):
    local['starting_state'] = starting_state
    exec(
        'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
        globals_scope,
        local,
    )
    transitions = local['transitions']
    data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
truth_data = np.array(data)

pred_prog_rules = [
    (0, [(0, (1, 1)), (0, (2, 1))]),
    (0, [(1, (0, 1)), (0, (2, 1))]),
    (0, [(1, (0, 1)), (0, (1, 1))]),
    (1, [(0, (2, 1))]),
    (2, [(1, (0, 1)), (0, (2, 1))]),
]
func_str = human_program(pred_prog_rules, '', num_vars, GENERATE_PYTHON)
globals_scope = {
    'generate_transition_steps': generate_transition_steps,
    'num_vars': num_vars,
    'seq_len': 2,
}
local = {}
exec(func_str, globals_scope, local)
data = []
for starting_state in range(2 ** (num_vars*delays)):
    local['starting_state'] = starting_state
    exec(
        'transitions = generate_transition_steps(generate_transition, seq_len, starting_state)',
        globals_scope,
        local,
    )
    transitions = local['transitions']
    #transitions[1][0] = False
    transitions[1][1] = True
    transitions[1][2] = True
    '''
    state = [
        True if starting_state&(1<<i) != 0 else False
        for i in range(num_vars*delays)
    ]
    transitions = [state, [False, False, True]]
    '''
    data.append([[1 if x else 0 for x in transitions[0]], [1 if x else 0 for x in transitions[1]]])
pred_data = np.array(data)
#print(truth_data.tolist())
#print(pred_data.tolist())
for i, s in enumerate(truth_data):
    print(s[0], s[1], pred_data[i][1])
print((np.square(pred_data[:,1,:num_vars] - truth_data[:,1,:num_vars]).mean()))
#print((np.square(pred_data[:,1] - truth_data[:,1]).mean()))
