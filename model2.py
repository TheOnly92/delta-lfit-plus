import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from tqdm import tqdm
from data import (
    generate_program,
    generate_transition_steps,
    human_program,
    GENERATE_PYTHON,
    subsumes,
    prog_subsumes,
)
import time


class TransformerModel(nn.Module):
    def __init__(self, ntoken, ninp, nhead, nhid, nlayers, dropout=0.5):
        super(TransformerModel, self).__init__()
        from torch.nn import TransformerEncoder, TransformerEncoderLayer
        self.model_type = 'Transformer'
        self.src_mask = None
        encoder_layers = TransformerEncoderLayer(ninp, nhead, nhid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, ninp)
        self.pos_encoder = PositionalEncoding(ninp, dropout)
        self.ninp = ninp
        self.ln_1 = nn.LayerNorm(nhid)
        self.ln_2 = nn.LayerNorm(nhid)
        self.decoder = nn.Linear(ninp, 1)
        self.dropout = nn.Dropout(p=dropout)
        self.nhid = nhid

        #self.init_weights()
        self.apply(self.init_weights)

    def _generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    def init_weights(self, module):
        if isinstance(module, (nn.Linear, nn.Embedding)):
            module.weight.data.normal_(mean=0.0, std=0.02)
            if isinstance(module, nn.Linear) and module.bias is not None:
                module.bias.data.zero_()
        elif isinstance(module, nn.LayerNorm):
            module.bias.data.zero_()
            module.weight.data.fill_(1.0)

    def forward(self, src):
        if self.src_mask is None or self.src_mask.size(0) != len(src):
            device = src.device
            mask = self._generate_square_subsequent_mask(len(src)).to(device)
            self.src_mask = mask

        src = self.encoder(src)# * math.sqrt(self.ninp)
        #src = self.pos_encoder(src)
        #src = self.ln_1(src)
        output = self.transformer_encoder(src)#, self.src_mask)
        #output = self.ln_1(output + src)
        #output = self.dropout(output)
        output = output[0,:]
        #output = output.permute(1, 0, 2)
        #output = torch.sum(output, dim=1)
        #output = torch.mean(output, dim=1)
        output = self.decoder(output)
        return output


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

num_vars = 7
time_steps = 10

dataset = []
seen = set()
generated = 0
seen_prog = set()
train_size = int(1e6)
val_size = 1000
test_size = 1000
count_0 = 0
count_1 = 0

pbar = tqdm(total=train_size + val_size + test_size)
while generated < train_size + val_size + test_size:
    state_n = []
    for _ in range(num_vars):
        state_n.append(np.random.randint(0, 2 ** time_steps))
    s = ':'.join([str(d) for d in state_n])
    if s in seen:
        continue
    seen.add(s)

    accept = True
    states = np.zeros((time_steps, num_vars))
    for j in range(num_vars):
        j_state = state_n[j]
        for k in range(time_steps):
            states[k][j] = 1 if (j_state & (1 << k)) > 0 else 0

    transitions = {}
    for j, st in enumerate(states[:-1]):
        st_next = states[j+1]
        st = ':'.join([str(d) for d in st])
        st_next = ':'.join([str(d) for d in st_next])
        if st in transitions:
            if transitions[st] != st_next:
                accept = False
                break
        else:
            transitions[st] = st_next

    add = True
    if accept:
        if count_1 < 0.55*train_size:
            count_1 += 1
        else:
            add = False
    else:
        if count_0 < 0.55*train_size:
            count_0 += 1
        else:
            add = False

    if add:
        state_n.append(2 ** time_steps + 1)
        dataset.append(([2 ** time_steps] + state_n, [1. if accept else 0.]))
        generated += 1
        pbar.update(1)

    '''
        p = []
        for v in range(num_vars):
            p.append((v, []))
        for tr_i in range(1, len(states)):
            e_i = states[tr_i-1]
            e_j = states[tr_i]
            for a in range(num_vars):
                if not e_j[a]:
                    rule = (a, [
                        (0 if e_i[b] else 1,
                         (b, 1))
                        for b in range(num_vars)
                    ])
                    conflicts = []
                    for r in p:
                        if r[0] == rule[0] and subsumes(r[1], rule[1]):
                            conflicts.append(r)
                    for r in conflicts:
                        p.remove(r)
                    for r in conflicts:
                        for l in rule[1]:
                            pos_l = (l[0], (l[1][0], 1))
                            neg_l = (0 if l[0] else 1, (l[1][0], 1))
                            if pos_l not in r[1] and neg_l not in r[1]:
                                r_c = (r[0], [(x[0], (x[1][0], 1)) for x in r[1]])
                                r_c[1].append(neg_l)
                                if not prog_subsumes(p, r_c):
                                    p = [
                                        r_p for r_p in p
                                        if (
                                                r_c[0] == r_p[0] and not subsumes(r_c[1], r_p[1])
                                        ) or r_c[0] != r_p[0]
                                    ]
                                    p.append(r_c)
        func_test = hash(human_program(p, '', num_vars, GENERATE_PYTHON))
        if func_test not in seen_prog:
            seen_prog.add(func_test)
            pbar.set_postfix({
                'seen_prog': len(seen_prog),
            })
    '''

pbar.close()

np.random.shuffle(dataset)
target_set = []
new_dataset = []
for data, target in dataset:
    new_dataset.append(data)
    target_set.append(target)
print(target_set[:10])
print(target_set.count([0.]))
dataset = torch.tensor(new_dataset).to(device)
target_set = torch.tensor(target_set).to(device)
train_data = dataset[:train_size]
train_target = target_set[:train_size]
val_data = dataset[train_size:train_size+val_size]
val_target = target_set[train_size:train_size+val_size]
test_data = dataset[train_size+val_size:]
test_target = target_set[train_size+val_size:]

batch_size = 64
eval_batch_size = 16

bptt = 14
debug = True
def get_batch(source, target_set, i, batch_size):
    global debug
    data = source[i*batch_size:(i+1)*batch_size]
    target = target_set[i*batch_size:(i+1)*batch_size]
    if debug:
        print(data[:10])
    data = data.permute(1, 0)
    if debug:
        print(data[:,:10])
        debug = False
    return data, target
    '''
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].view(-1)
    return data, target
    '''

ntokens = 2 ** time_steps + 2
emsize = 128
nhid = 128
nlayers = 4
nhead = 4
dropout = 0.1
model = TransformerModel(ntokens, emsize, nhead, nhid, nlayers, dropout).to(device)

no_decay = ['bias', 'LayerNorm.weight']
params_decay = [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)]
params_nodecay = [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)]

#criterion = nn.CrossEntropyLoss()
criterion = nn.BCEWithLogitsLoss()
lr = 1e-4
#optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=0.1)
optimizer = torch.optim.AdamW(model.parameters(), betas=[0.9, 0.95], weight_decay=0.1)
#optimizer = torch.optim.AdamW([
#    {'params': params_decay, 'weight_decay': 0.1},
#    {'params': params_nodecay, 'weight_decay': 0.0},
#], lr=lr, betas=(0.9, 0.95))
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 0.005, gamma=0.8)

def train():
    model.train()
    total_loss = 0.
    start_time = time.time()
    #ntokens = len(TEXT.vocab.stoi)
    log_interval = max((len(train_data) // batch_size) // 10, 1)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, batch_size)):
        data, targets = get_batch(train_data, train_target, batch, batch_size)
        optimizer.zero_grad()
        output = model(data)
        #print('===============')
        #print(data.shape)
        #print(data)
        #print(output.argmax(1))
        #print(targets)
        loss = criterion(output, targets)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        optimizer.step()

        total_loss += loss.item()
        if batch % log_interval == 0 and batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            #print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, scheduler.get_lr()[0], elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            print('| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.2f} | ppl {:8.2f}'.format(epoch, batch, len(train_data) // batch_size, elapsed * 1000 / log_interval, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()


def evaluate(eval_model, data_source, target_set, do_output=False):
    eval_model.eval()
    total_loss = 0.
    correct = 0
    total = 0
    with torch.no_grad():
        for i in range(max(data_source.size(0) // eval_batch_size, 1)):
            data, targets = get_batch(data_source, target_set, i, eval_batch_size)
            output = eval_model(data)
            if do_output:
                print('input', data[:,0])
                print('target', targets[0])
                #print('output', output.argmax(1)[0])
                print('output', torch.round(torch.sigmoid(output[0])))
                #correct += (targets == output.argmax(1)).sum().item()
                correct += (targets == torch.round(torch.sigmoid(output))).sum().item()
                total += targets.size(0)
            elif i == 0:
                print(torch.sigmoid(output[:10]))
                #print(output.argmax(1)[:10])
                print(torch.round(torch.sigmoid(output[:10]).view(-1)))
            total_loss += len(data) * criterion(output, targets).item()
    if do_output:
        print(f'Accuracy is {100*correct/total:.2f}%')
    return total_loss / (len(data_source) - 1)


best_val_loss = float("inf")
epochs = 50
best_model = None

test_loss = evaluate(model, test_data, test_target)
print('=' * 89)
print('| Pre training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
print('=' * 89)

try:
    for epoch in range(1, epochs + 1):
        epoch_start_time = time.time()
        train()
        val_loss = evaluate(model, val_data, val_target)
        print('-' * 89)
        print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time), val_loss, math.exp(val_loss)))
        print('-' * 89)

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_model = model

        #scheduler.step()
except KeyboardInterrupt:
    pass

if best_model:
    test_loss = evaluate(best_model, test_data, test_target, True)
    print('=' * 89)
    print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(test_loss, math.exp(test_loss)))
    print('=' * 89)
    torch.save(best_model, 'best_model')
